package com.ferick.structures.graphs;

public class VectorItem {

    private final int vertexIndex;
    private int weight;

    public VectorItem(int vertexIndex, int weight) {
        this.vertexIndex = vertexIndex;
        this.weight = weight;
    }

    public VectorItem(int vertexIndex) {
        this(vertexIndex, 0);
    }

    public int getVertexIndex() {
        return vertexIndex;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
