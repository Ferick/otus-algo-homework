package com.ferick.structures.graphs;

import com.ferick.structures.arrays.VectorArray;

import java.util.Arrays;
import java.util.stream.IntStream;

public class AdjacencyVector {

    private final VectorArray<VectorArray<VectorItem>> vectorArray = new VectorArray<>();

    public AdjacencyVector(int number) {
        IntStream.range(0, number).forEach(i -> vectorArray.add(new VectorArray<>()));
    }

    public void addVectorItems(int vertexIndex, VectorItem... vectorItems) {
        checkArrayBounds(vertexIndex);
        VectorArray<VectorItem> vertexVector = vectorArray.get(vertexIndex);
        Arrays.stream(vectorItems).forEach(vertexVector::add);
    }

    public VectorArray<VectorItem> getVectors(int vertexIndex) {
        checkArrayBounds(vertexIndex);
        return vectorArray.get(vertexIndex);
    }

    public AdjacencyVector invertDirections() {
        AdjacencyVector newVector = new AdjacencyVector(vectorArray.size());
        IntStream.range(0, vectorArray.size()).forEach(currentIndex -> {
            VectorArray<VectorItem> vectorItems = vectorArray.get(currentIndex);
            for (int i = 0; i < vectorItems.size(); i++) {
                VectorItem vectorItem = vectorItems.get(i);
                VectorItem direction = getVertexDirectionTo(vectorItem.getVertexIndex(), currentIndex);
                if (direction != null && currentIndex < vectorItem.getVertexIndex()) {
                    newVector.vectorArray.get(currentIndex).add(vectorItem);
                    newVector.vectorArray.get(vectorItem.getVertexIndex()).add(direction);
                } else if (direction == null) {
                    newVector.vectorArray.get(vectorItem.getVertexIndex())
                            .add(new VectorItem(currentIndex, vectorItem.getWeight()));
                }
            }
        });
        return newVector;
    }

    public VectorArray<Edge> getEdgesFromNonDirectedGraph() {
        VectorArray<Edge> edges = new VectorArray<>();
        IntStream.range(0, vectorArray.size()).forEach(currentIndex -> {
            VectorArray<VectorItem> vectorItems = vectorArray.get(currentIndex);
            for (int i = 0; i < vectorItems.size(); i++) {
                VectorItem vectorItem = vectorItems.get(i);
                VectorItem direction = getVertexDirectionTo(vectorItem.getVertexIndex(), currentIndex);
                if (direction != null) {
                    addEdgeIfAbsent(edges, vectorItem, direction);
                } else {
                    throw new IllegalStateException("Graph must be non-directed");
                }
            }
        });
        return edges;
    }

    public int size() {
        return vectorArray.size();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(this.getClass().getSimpleName() + "\n");
        for (int i = 0; i < vectorArray.size(); i++) {
            builder.append("Vertex ").append(i).append(":\n");
            VectorArray<VectorItem> vectorItems = vectorArray.get(i);
            if (vectorItems.size() == 0) {
                builder.append("empty");
            } else {
                for (int j = 0; j < vectorItems.size(); j++) {
                    VectorItem vectorItem = vectorItems.get(j);
                    builder.append(" -(").append(vectorItem.getWeight()).append(")-> ")
                            .append(vectorItem.getVertexIndex());
                }
            }
            builder.append('\n');
        }
        return builder.toString();
    }

    private VectorItem getVertexDirectionTo(int firstVertexIndex, int secondVertexIndex) {
        VectorArray<VectorItem> vectorItems = getVectors(firstVertexIndex);
        for (int i = 0; i < vectorItems.size(); i++) {
            VectorItem vectorItem = vectorItems.get(i);
            if (vectorItem.getVertexIndex() == secondVertexIndex) {
                return vectorItem;
            }
        }
        return null;
    }

    private void addEdgeIfAbsent(VectorArray<Edge> array, VectorItem vectorOne, VectorItem vectorTwo) {
        int vertexOne = vectorOne.getVertexIndex();
        int vertexTwo = vectorTwo.getVertexIndex();
        if (vectorOne.getWeight() != vectorTwo.getWeight()) {
            throw new IllegalStateException(
                    "Weights between vertex " + vertexOne + " and vertex " + vertexTwo + " does not equal"
            );
        }
        Edge edge = new Edge(vertexOne, vertexTwo, vectorOne.getWeight());
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i).equals(edge)) {
                return;
            }
        }
        array.add(edge);
    }

    private void checkArrayBounds(int vertexIndex) {
        if (vertexIndex < 0 || vertexIndex >= vectorArray.size()) {
            throw new IllegalArgumentException("Vector index must be in range 0 .. " + (vectorArray.size() - 1));
        }
    }
}
