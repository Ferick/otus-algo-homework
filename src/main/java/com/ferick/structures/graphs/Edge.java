package com.ferick.structures.graphs;

public class Edge {

    private final int vertexOne;
    private final int vertexTwo;
    private int weight;

    public Edge(int vertexOne, int vertexTwo, int weight) {
        this.vertexOne = vertexOne;
        this.vertexTwo = vertexTwo;
        this.weight = weight;
    }

    public Edge(int vertexOne, int vertexTwo) {
        this(vertexOne, vertexTwo, 0);
    }

    public int getVertexOne() {
        return vertexOne;
    }

    public int getVertexTwo() {
        return vertexTwo;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        if (
                (vertexOne == edge.vertexOne && vertexTwo == edge.vertexTwo) ||
                (vertexOne == edge.vertexTwo && vertexTwo == edge.vertexOne)
        ) {
            return weight == edge.weight;
        }

        return false;
    }

    @Override
    public int hashCode() {
        int result = vertexOne;
        result = 31 * result + vertexTwo;
        result = 31 * result + weight;
        return result;
    }
}
