package com.ferick.structures.trees;

public interface BasicTree<K extends Comparable<K>, V> {

    void put(K key, V value);

    V get(K key);

    void delete(K key);

    int size();
}
