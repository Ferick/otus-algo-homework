package com.ferick.structures.trees.avl;

import com.ferick.structures.trees.BasicTree;

public class AvlTree<K extends Comparable<K>, V> implements BasicTree<K, V> {

    private AvlTreeNode<K, V> root;
    private int size = 0;

    @Override
    public void put(K key, V value) {
        AvlTreeNode<K, V> newNode = new AvlTreeNode<>(key, value);
        if (root == null) {
            root = newNode;
        } else {
            insert(root, newNode);
        }
        size++;
    }

    @Override
    public V get(K key) {
        if (root == null) {
            return null;
        }
        AvlTreeNode<K, V> result = search(root, key);
        if (result != null) {
            return result.getValue();
        }
        return null;
    }

    @Override
    public void delete(K key) {
        if (root != null) {
            if (root.isLeaf() && root.getKey().equals(key)) {
                root = null;
                size--;
            } else {
                AvlTreeNode<K, V> toBeRemoved = search(root, key);
                if (toBeRemoved != null) {
                    remove(toBeRemoved);
                    size--;
                }
            }
        }
    }

    @Override
    public int size() {
        return size;
    }

    private void insert(AvlTreeNode<K, V> parentNode, AvlTreeNode<K, V> newNode) {
        if (newNode.equalTo(parentNode)) {
            parentNode.setValue(newNode.getValue());
        } else if (newNode.lessThan(parentNode)) {
            if (parentNode.leftAbsent()) {
                parentNode.setLeft(newNode);
                newNode.setParent(parentNode);
            } else {
                insert(parentNode.getLeft(), newNode);
            }
            balance(newNode);
        } else {
            if (parentNode.rightAbsent()) {
                parentNode.setRight(newNode);
                newNode.setParent(parentNode);
            } else {
                insert(parentNode.getRight(), newNode);
            }
            balance(newNode);
        }
    }

    private AvlTreeNode<K, V> search(AvlTreeNode<K, V> parentNode, K key) {
        if (parentNode.getKey().equals(key)) {
            return parentNode;
        } else if (parentNode.greaterThan(key)) {
            if (parentNode.leftAbsent()) {
                return null;
            } else {
                return search(parentNode.getLeft(), key);
            }
        } else {
            if (parentNode.rightAbsent()) {
                return null;
            } else {
                return search(parentNode.getRight(), key);
            }
        }
    }

    private void remove(AvlTreeNode<K, V> node) {
        if (node.isLeaf()) {
            removeLeaf(node);
        } else if (node.hasOnlyOneChild()) {
            removeNodeWithOneChild(node);
        } else {
            removeNodeWithTwoChildren(node);
        }
    }

    private void removeLeaf(AvlTreeNode<K, V> leaf) {
        AvlTreeNode<K, V> parent = leaf.getParent();
        if (parent.greaterThan(leaf)) {
            parent.removeLeft();
        } else {
            parent.removeRight();
        }
        balance(parent);
    }

    private void removeNodeWithOneChild(AvlTreeNode<K, V> node) {
        AvlTreeNode<K, V> parent = node.getParent();
        AvlTreeNode<K, V> childNode = node.leftAbsent() ? node.getRight() : node.getLeft();
        if (parent.greaterThan(node)) {
            parent.setLeft(childNode);
        } else {
            parent.setRight(childNode);
        }
        childNode.setParent(parent);
        balance(parent);
    }

    private void removeNodeWithTwoChildren(AvlTreeNode<K, V> node) {
        AvlTreeNode<K, V> maxFromLeft = getMaxFromLeft(node);
        K nodeKey = node.getKey();
        K maxFromLeftParentKey = maxFromLeft.getParent().getKey();
        node.setKey(maxFromLeft.getKey());
        node.setValue(maxFromLeft.getValue());
        AvlTreeNode<K, V> maxFromLeftParent = maxFromLeft.getParent();
        if (maxFromLeft.leftAbsent()) {
            if (nodeKey.equals(maxFromLeftParentKey)) {
                maxFromLeftParent.removeLeft();
            } else {
                maxFromLeftParent.removeRight();
            }
        } else {
            maxFromLeft.getLeft().setParent(maxFromLeftParent);
            if (nodeKey.equals(maxFromLeftParentKey)) {
                maxFromLeftParent.setLeft(maxFromLeft.getLeft());
            } else {
                maxFromLeftParent.setRight(maxFromLeft.getLeft());
            }
        }
        balance(maxFromLeftParent);
    }

    private AvlTreeNode<K, V> getMaxFromLeft(AvlTreeNode<K, V> node) {
        AvlTreeNode<K, V> left = node.getLeft();
        if (left.rightAbsent()) {
            return left;
        } else {
            AvlTreeNode<K, V> max = left.getRight();
            while (!max.rightAbsent()) {
                max = max.getRight();
            }
            return max;
        }
    }

    private void balance(AvlTreeNode<K, V> node) {
        AvlTreeNode<K, V> parent = node.getParent();
        if (parent == null) {
            return;
        }
        int leftHeight = (parent.leftAbsent()) ? 0 : parent.getLeft().getHeight();
        int rightHeight = (parent.rightAbsent()) ? 0 : parent.getRight().getHeight();
        if (leftHeight > (rightHeight + 1)) {
            rightBalance(parent);
        } else if (rightHeight > (leftHeight + 1)) {
            leftBalance(parent);
        }
    }

    private void rightBalance(AvlTreeNode<K, V> node) {
        AvlTreeNode<K, V> left = node.getLeft();
        AvlTreeNode<K, V> parent;
        if (left.getLeftChildHeight() < left.getRightChildHeight()) {
            leftSmallRotation(left);
        }
        parent = rightSmallRotation(node);
        if (parent != null) {
            balance(parent);
        }
    }

    private void leftBalance(AvlTreeNode<K, V> node) {
        AvlTreeNode<K, V> right = node.getRight();
        AvlTreeNode<K, V> parent;
        if (right.getRightChildHeight() < right.getLeftChildHeight()) {
            rightSmallRotation(right);
        }
        parent = leftSmallRotation(node);
        if (parent != null) {
            balance(parent);
        }
    }

    private AvlTreeNode<K, V> rightSmallRotation(AvlTreeNode<K, V> node) {
        AvlTreeNode<K, V> parent = node.getParent();
        AvlTreeNode<K, V> left = node.getLeft();
        AvlTreeNode<K, V> cTree = left.getRight();

        if (parent == null) {
            root = left;
            left.setParent(null);
        } else {
            left.setParent(parent);
            if (parent.greaterThan(node)) {
                parent.setLeft(left);
            } else {
                parent.setRight(left);
            }
        }

        left.setRight(node);
        node.setParent(left);
        node.setLeft(cTree);
        if (cTree != null) {
            cTree.setParent(node);
        }

        return parent;
    }

    private AvlTreeNode<K, V> leftSmallRotation(AvlTreeNode<K, V> node) {
        AvlTreeNode<K, V> parent = node.getParent();
        AvlTreeNode<K, V> right = node.getRight();
        AvlTreeNode<K, V> cTree = right.getLeft();

        if (parent == null) {
            root = right;
            right.setParent(null);
        } else {
            right.setParent(parent);
            if (parent.greaterThan(node)) {
                parent.setLeft(right);
            } else {
                parent.setRight(right);
            }
        }

        right.setLeft(node);
        node.setParent(right);
        node.setRight(cTree);
        if (cTree != null) {
            cTree.setParent(node);
        }

        return parent;
    }
}
