package com.ferick.structures.trees.avl;

public class AvlTreeNode<K extends Comparable<K>, V> {

    private K key;
    private V value;
    private AvlTreeNode<K, V> left;
    private AvlTreeNode<K, V> right;
    private AvlTreeNode<K, V> parent;

    public AvlTreeNode(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public AvlTreeNode<K, V> getLeft() {
        return left;
    }

    public void setLeft(AvlTreeNode<K, V> left) {
        this.left = left;
    }

    public AvlTreeNode<K, V> getRight() {
        return right;
    }

    public void setRight(AvlTreeNode<K, V> right) {
        this.right = right;
    }

    public AvlTreeNode<K, V> getParent() {
        return parent;
    }

    public void setParent(AvlTreeNode<K, V> parent) {
        this.parent = parent;
    }

    public int getHeight() {
        if (isLeaf()) {
            return 1;
        } else {
            return maxChildHeight() + 1;
        }
    }

    boolean equalTo(AvlTreeNode<K, V> other) {
        return this.getKey().compareTo(other.getKey()) == 0;
    }

    boolean lessThan(AvlTreeNode<K, V> other) {
        return this.getKey().compareTo(other.getKey()) < 0;
    }

    boolean greaterThan(AvlTreeNode<K, V> other) {
        return this.getKey().compareTo(other.getKey()) > 0;
    }

    boolean lessThan(K key) {
        return this.getKey().compareTo(key) < 0;
    }

    boolean greaterThan(K key) {
        return this.getKey().compareTo(key) > 0;
    }

    boolean leftAbsent() {
        return left == null;
    }

    boolean rightAbsent() {
        return right == null;
    }

    boolean isLeaf() {
        return leftAbsent() && rightAbsent();
    }

    boolean hasOnlyOneChild() {
        return (leftAbsent() && !rightAbsent()) || (!leftAbsent() && rightAbsent());
    }

    void removeLeft() {
        left = null;
    }

    void removeRight() {
        right = null;
    }

    int getLeftChildHeight() {
        return (leftAbsent()) ? 0 : left.getHeight();
    }

    int getRightChildHeight() {
        return (rightAbsent()) ? 0 : right.getHeight();
    }

    private int maxChildHeight() {
        int leftHeight = (leftAbsent()) ? 0 : left.getHeight();
        int rightHeight = (rightAbsent()) ? 0 : right.getHeight();
        return Math.max(leftHeight, rightHeight);
    }
}
