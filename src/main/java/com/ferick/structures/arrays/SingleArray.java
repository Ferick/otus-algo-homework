package com.ferick.structures.arrays;

public class SingleArray<T> extends BasicArray<T> {

    public SingleArray() {
        super(0);
    }

    @Override
    public void add(T item) {
        resize();
        array[size() - 1] = item;
    }

    @Override
    public void add(int index, T item) {
        checkBoundaries(index);
        T[] newArray = createNewArray();
        for (int i = 0; i <= size(); i++) {
            if (i < index) {
                newArray[i] = array[i];
            } else if (i == index) {
                newArray[i] = item;
            } else {
                newArray[i] = array[i - 1];
            }
        }
        array = newArray;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T remove(int index) {
        checkBoundaries(index);
        T[] newArray = (T[]) new Object[size() - 1];
        T removedElement = null;
        for (int i = 0; i < size(); i++) {
            if (i < index) {
                newArray[i] = array[i];
            } else if (i == index) {
                removedElement = array[i];
            } else {
                newArray[i - 1] = array[i];
            }
        }
        array = newArray;
        return removedElement;
    }

    @Override
    public int size() {
        return array.length;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected T[] createNewArray() {
        return (T[]) new Object[size() + 1];
    }
}
