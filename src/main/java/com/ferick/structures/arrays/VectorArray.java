package com.ferick.structures.arrays;

public class VectorArray<T> extends BasicArray<T> {

    private static final int VECTOR = 10;
    private int size = 0;

    public VectorArray() {
        super(0);
    }

    @Override
    public void add(T item) {
        if (size() == array.length) {
            resize();
        }
        array[size()] = item;
        size++;
    }

    @Override
    public void add(int index, T item) {
        checkBoundaries(index);
        if (size() == array.length) {
            T[] newArray = createNewArray();
            for (int i = 0; i <= size(); i++) {
                if (i < index) {
                    newArray[i] = array[i];
                } else if (i == index) {
                    newArray[i] = item;
                } else {
                    newArray[i] = array[i - 1];
                }
            }
            array = newArray;
        } else {
            for (int i = size(); i >= index; i--) {
                if (i == index) {
                    array[i] = item;
                } else {
                    array[i] = array[i - 1];
                }
            }
        }
        size++;
    }

    @Override
    public T remove(int index) {
        checkBoundaries(index);
        T removedElement = null;
        for (int i = index; i < size(); i++) {
            if (i == index) {
                removedElement = array[i];
            }
            if (i == size() - 1) {
                array[i] = null;
            } else {
                array[i] = array[i + 1];
            }
        }
        size--;
        return removedElement;
    }

    @Override
    public int size() {
        return size;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected T[] createNewArray() {
        return (T[]) new Object[size() + VECTOR];
    }
}
