package com.ferick.structures.arrays.space;

public class SpaceArrayItem<T> {

    private static final int FULL_SIZE = 100;
    private final T[] array;
    private int size;

    @SuppressWarnings("unchecked")
    public SpaceArrayItem() {
        array = (T[]) new Object[FULL_SIZE];
    }

    public void add(T item) {
        array[size()] = item;
        size++;
    }

    public T get(int index) {
        if (isEmpty()) {
            return null;
        } else if (index < size()) {
            return array[index];
        }
        return null;
    }

    public T addAndReturnExceeding(int index, T item) {
        checkBoundaries(index);
        T exceedingElement = null;
        if (isFull()) {
            for (int i = size() - 1; i >= index; i--) {
                if (i == size() - 1) {
                    exceedingElement = array[i];
                }
                if (i == index) {
                    array[i] = item;
                } else {
                    array[i] = array[i - 1];
                }
            }
            return exceedingElement;
        } else {
            for (int i = size(); i >= index; i--) {
                if (i == index) {
                    array[i] = item;
                } else {
                    array[i] = array[i - 1];
                }
            }
            size++;
            return null;
        }
    }

    public T remove(int index) {
        checkBoundaries(index);
        T removedElement = null;
        for (int i = index; i < size(); i++) {
            if (i == index) {
                removedElement = array[i];
            }
            if (i == size() - 1) {
                array[i] = null;
            } else {
                array[i] = array[i + 1];
            }
        }
        size--;
        return removedElement;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

    public boolean isHalfFull() {
        return size() >= FULL_SIZE / 2;
    }

    public boolean isFull() {
        return size() == FULL_SIZE;
    }

    private void checkBoundaries(int index) {
        if (index >= size()) {
            throw new IllegalArgumentException("Index " + index + " is greater than array size " + size());
        }
    }
}
