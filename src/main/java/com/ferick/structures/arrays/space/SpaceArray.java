package com.ferick.structures.arrays.space;

import com.ferick.structures.arrays.Array;
import com.ferick.structures.arrays.VectorArray;
import kotlin.Pair;

public class SpaceArray<T> implements Array<T> {

    private final VectorArray<SpaceArrayItem<T>> arrayList;
    private int totalSize = 0;

    public SpaceArray() {
        arrayList = new VectorArray<>();
    }

    @Override
    public void add(T item) {
        SpaceArrayItem<T> lastArrayItem = getLastArrayItem();
        if (lastArrayItem.isHalfFull()) {
            SpaceArrayItem<T> newArrayItem = new SpaceArrayItem<>();
            newArrayItem.add(item);
            arrayList.add(newArrayItem);
        } else {
            lastArrayItem.add(item);
        }
        totalSize++;
    }

    @Override
    public T get(int index) {
        Pair<Integer, Integer> address = getAddressByIndex(index);
        SpaceArrayItem<T> arrayItem = arrayList.get(address.getFirst());
        return arrayItem.get(address.getSecond());
    }

    @Override
    public void add(int index, T item) {
        Pair<Integer, Integer> address = getAddressByIndex(index);
        SpaceArrayItem<T> arrayItem = arrayList.get(address.getFirst());
        T exceedingItem = arrayItem.addAndReturnExceeding(address.getSecond(), item);
        int nextArrayIndex = address.getFirst();
        while (exceedingItem != null) {
            if (hasNextArrayItem(nextArrayIndex)) {
                SpaceArrayItem<T> nextArrayItem = getNextArrayItem(nextArrayIndex);
                exceedingItem = nextArrayItem.addAndReturnExceeding(0, exceedingItem);
                nextArrayIndex++;
            } else {
                SpaceArrayItem<T> newArrayItem = new SpaceArrayItem<>();
                newArrayItem.add(exceedingItem);
                arrayList.add(newArrayItem);
            }
        }
        totalSize++;
    }

    @Override
    public T remove(int index) {
        Pair<Integer, Integer> address = getAddressByIndex(index);
        SpaceArrayItem<T> arrayItem = arrayList.get(address.getFirst());
        return arrayItem.remove(address.getSecond());
    }

    @Override
    public int size() {
        return totalSize;
    }

    @Override
    public boolean isEmpty() {
        return arrayList.isEmpty();
    }

    private Pair<Integer, Integer> getAddressByIndex(int index) {
        if (index >= totalSize) {
            throw new IllegalArgumentException("Index " + index + " is greater than array size " + size());
        } else if (isEmpty()) {
            throw new IllegalStateException("Array is empty");
        }
        int arrayIndex = 0;
        int itemIndexInArray = 0;
        int sizes = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            int arrayItemSize = arrayList.get(i).size();
            if (index >= (sizes + arrayItemSize)) {
                sizes += arrayItemSize;
            } else {
                arrayIndex = i;
                itemIndexInArray = index - sizes;
                break;
            }
        }
        return new Pair<>(arrayIndex, itemIndexInArray);
    }

    private SpaceArrayItem<T> getLastArrayItem() {
        if (arrayList.isEmpty()) {
            arrayList.add(new SpaceArrayItem<>());
        }
        return arrayList.get(arrayList.size() - 1);
    }

    private boolean hasNextArrayItem(int arrayIndex) {
        return (arrayIndex + 1) < arrayList.size();
    }

    private SpaceArrayItem<T> getNextArrayItem(int arrayIndex) {
        return arrayList.get(arrayIndex + 1);
    }
}
