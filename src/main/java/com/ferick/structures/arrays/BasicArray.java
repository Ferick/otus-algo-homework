package com.ferick.structures.arrays;

public abstract class BasicArray<T> implements Array<T> {

    protected T[] array;

    @SuppressWarnings("unchecked")
    public BasicArray(int size) {
        array = (T[]) new Object[size];
    }

    protected abstract T[] createNewArray();

    @Override
    public T get(int index) {
        if (isEmpty()) {
            return null;
        } else if (index < size()) {
            return array[index];
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    protected void resize() {
        T[] newArray = createNewArray();
        if (size() >= 0) System.arraycopy(array, 0, newArray, 0, size());
        array = newArray;
    }

    protected void checkBoundaries(int index) {
        if (index >= size()) {
            throw new IllegalArgumentException("Index " + index + " is greater than array size " + size());
        }
    }
}
