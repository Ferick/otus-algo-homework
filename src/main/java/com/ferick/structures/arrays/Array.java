package com.ferick.structures.arrays;

public interface Array<T> {

    void add(T item);
    T get(int index);
    void add(int index, T item);
    T remove(int index);
    int size();
    boolean isEmpty();
}
