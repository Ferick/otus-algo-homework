package com.ferick.structures.maps;

class BucketItem<K, V> {

    private final SimpleMapEntry<K, V> entry;
    private BucketItem<K, V> next;

    BucketItem(SimpleMapEntry<K, V> entry) {
        this.entry = entry;
    }

    SimpleMapEntry<K, V> getEntry() {
        return entry;
    }

    BucketItem<K, V> getNext() {
        return next;
    }

    void setNext(BucketItem<K, V> next) {
        this.next = next;
    }

    boolean hasNext() {
        return next != null;
    }
}
