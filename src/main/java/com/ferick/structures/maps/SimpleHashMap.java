package com.ferick.structures.maps;

public class SimpleHashMap<K, V> {

    private static final int INITIAL_BUCKET_SIZE = 10;

    private LinkedBucket<K, V>[] buckets;
    private int size = 0;

    @SuppressWarnings("unchecked")
    public SimpleHashMap() {
        this.buckets = new LinkedBucket[INITIAL_BUCKET_SIZE];
    }

    public void put(K key, V value) {
        if (size >= buckets.length) {
            resize();
        }
        boolean added = insert(buckets, key, value);
        if (added) {
            size++;
        }
    }

    public V get(K key) {
        if (size == 0) {
            return null;
        }
        int hash = hash(key.hashCode(), buckets.length);
        LinkedBucket<K, V> bucket = buckets[hash];
        if (bucket != null) {
            return (bucket.get(key) != null) ? bucket.get(key).getValue() : null;
        }
        return null;
    }

    public void remove(K key) {
        if (size == 0) {
            return;
        }
        int hash = hash(key.hashCode(), buckets.length);
        LinkedBucket<K, V> bucket = buckets[hash];
        if (bucket != null) {
            bucket.delete(key);
            size--;
        }
    }

    public int size() {
        return size;
    }

    private int hash(int hashCode, int bucketsLength) {
        return Math.abs(hashCode) % bucketsLength;
    }

    @SuppressWarnings("unchecked")
    private void resize() {
        LinkedBucket<K, V>[] newBuckets = new LinkedBucket[buckets.length * 2];
        for (LinkedBucket<K, V> bucket : buckets) {
            if (bucket != null) {
                for (SimpleMapEntry<K, V> entry : bucket.getAll()) {
                    insert(newBuckets, entry.getKey(), entry.getValue());
                }
            }
        }
        buckets = newBuckets;
    }

    private boolean insert(LinkedBucket<K, V>[] array, K key, V value) {
        int hash = hash(key.hashCode(), array.length);
        LinkedBucket<K, V> bucket = array[hash];
        if (bucket == null) {
            bucket = new LinkedBucket<>();
            array[hash] = bucket;
        }
        SimpleMapEntry<K, V> entry = bucket.get(key);
        if (entry != null) {
            entry.setValue(value);
        } else {
            bucket.put(new SimpleMapEntry<>(key, value));
            return true;
        }
        return false;
    }
}
