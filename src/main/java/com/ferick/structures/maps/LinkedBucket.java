package com.ferick.structures.maps;

public class LinkedBucket<K, V> {

    private BucketItem<K, V> first;
    private BucketItem<K, V> last;
    private int size = 0;

    public void put(SimpleMapEntry<K, V> newEntry) {
        BucketItem<K, V> newItem = new BucketItem<>(newEntry);
        if (last == null) {
            first = newItem;
        } else {
            last.setNext(newItem);
        }
        last = newItem;
        size++;
    }

    public SimpleMapEntry<K, V> get(K key) {
        if (size == 0) {
            return null;
        }
        int count = size;
        BucketItem<K, V> item = first;
        while (count > 0) {
            if (item.getEntry().getKey().equals(key)) {
                return item.getEntry();
            } else {
                item = item.getNext();
                count--;
            }
        }
        return null;
    }

    public void delete(K key) {
        if (size == 0) {
            return;
        } else if (size == 1) {
            if (first.getEntry().getKey().equals(key)) {
                first = null;
                last = null;
                size--;
            } else {
                return;
            }
        }
        int count = size;
        BucketItem<K, V> previous = null;
        BucketItem<K, V> current = first;
        while (count > 0) {
            if (current.getEntry().getKey().equals(key)) {
                if (previous == null) {
                    first = current.getNext();
                } else {
                    previous.setNext(current.getNext());
                    size--;
                    return;
                }
            } else {
                previous = current;
                current = current.getNext();
                count--;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public SimpleMapEntry<K, V>[] getAll() {
        SimpleMapEntry<K, V>[] array = new SimpleMapEntry[size];
        if (size > 0) {
            int count = 0;
            array[count++] = first.getEntry();
            BucketItem<K, V> current = first;
            while (current.hasNext()) {
                array[count++] = current.getNext().getEntry();
                current = current.getNext();
            }
        }
        return array;
    }
}
