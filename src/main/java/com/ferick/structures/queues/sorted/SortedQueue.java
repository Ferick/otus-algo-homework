package com.ferick.structures.queues.sorted;

import com.ferick.structures.queues.QueueItem;
import com.ferick.structures.queues.SimpleQueue;

public class SortedQueue<T extends Comparable<T>> extends SimpleQueue<T> {

    @Override
    public void put(T value) {
        QueueItem<T> newItem = new QueueItem<>(value);
        if (isEmpty()) {
            first = newItem;
        } else {
            putInOrder(newItem);
        }
        size++;
    }

    private void putInOrder(QueueItem<T> newItem) {
        if (newItem.getValue().compareTo(first.getValue()) < 0) {
            newItem.setNext(first);
            first = newItem;
        } else {
            QueueItem<T> previous = first;
            QueueItem<T> next = first.getNext();
            if (next == null) {
                first.setNext(newItem);
                return;
            }
            boolean finished = false;
            while (!finished) {
                if (newItem.getValue().compareTo(next.getValue()) < 0) {
                    newItem.setNext(next);
                    previous.setNext(newItem);
                    break;
                } else {
                    if (next.hasNext()) {
                        previous = next;
                        next = next.getNext();
                    } else {
                        next.setNext(newItem);
                        finished = true;
                    }
                }
            }
        }
    }
}
