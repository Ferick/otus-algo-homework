package com.ferick.structures.queues;

public class SimpleQueue<T> {

    protected QueueItem<T> first;
    protected QueueItem<T> last;
    protected int size = 0;

    public void put(T value) {
        QueueItem<T> newItem = new QueueItem<>(value);
        if (isEmpty()) {
            first = newItem;
        } else {
            last.setNext(newItem);
        }
        last = newItem;
        size++;
    }

    public T take() {
        if (!isEmpty()) {
            T value = first.getValue();
            if (first.hasNext()) {
                first = first.getNext();
            } else {
                first = null;
            }
            size--;
            return value;
        }
        return null;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return size;
    }
}
