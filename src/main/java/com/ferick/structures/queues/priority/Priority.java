package com.ferick.structures.queues.priority;

public enum Priority {

    FIRST("Я только спросить"),
    SECOND("По записи"),
    THIRD("Повторный визит"),
    FOURTH("Лузеры");

    private final String description;

    Priority(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
