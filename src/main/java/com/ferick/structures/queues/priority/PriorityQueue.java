package com.ferick.structures.queues.priority;

import com.ferick.structures.arrays.SingleArray;
import com.ferick.structures.queues.SimpleQueue;

/**
 * Priority calculates from the order in the priority array,
 * the first element in the priority array has the highest priority.
 *
 * @param <P> priority
 * @param <T> value
 */
public class PriorityQueue<P, T> {

    private final SingleArray<PriorityItem<P, SimpleQueue<T>>> array;

    public PriorityQueue(P[] priorities) {
        array = new SingleArray<>();
        for (P priority : priorities) {
            array.add(new PriorityItem<>(priority, new SimpleQueue<>()));
        }
    }

    public void enqueue(P priority, T item) {
        for (int i = 0; i < array.size(); i++) {
            PriorityItem<P, SimpleQueue<T>> priorityItem = array.get(i);
            if (priorityItem.getPriority().equals(priority)) {
                priorityItem.getQueue().put(item);
                return;
            }
        }
    }

    public T dequeue() {
        for (int i = 0; i < array.size(); i++) {
            PriorityItem<P, SimpleQueue<T>> priorityItem = array.get(i);
            if (!priorityItem.getQueue().isEmpty()) {
                return priorityItem.getQueue().take();
            }
        }
        return null;
    }
}
