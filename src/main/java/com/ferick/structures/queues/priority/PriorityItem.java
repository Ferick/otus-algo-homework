package com.ferick.structures.queues.priority;

public class PriorityItem<P, Q> {

    private final P priority;
    private final Q queue;

    public PriorityItem(P priority, Q queue) {
        this.priority = priority;
        this.queue = queue;
    }

    public P getPriority() {
        return priority;
    }

    public Q getQueue() {
        return queue;
    }
}
