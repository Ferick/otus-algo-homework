package com.ferick.structures.queues;

public class QueueItem<T> {

    private final T value;
    private QueueItem<T> next;

    public QueueItem(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setNext(QueueItem<T> next) {
        this.next = next;
    }

    public boolean hasNext() {
        return next != null;
    }

    public QueueItem<T> getNext() {
        return next;
    }
}
