package com.ferick.structures.tries;

import com.ferick.structures.maps.LinkedBucket;
import com.ferick.structures.maps.SimpleMapEntry;
import org.jetbrains.annotations.Nullable;

public class Trie<V> {

    private final LinkedBucket<Character, TrieNode<V>> trieBucket = new LinkedBucket<>();

    public void put(String key, V value) {
        put(key.toCharArray(), value);
    }

    @Nullable
    V get(String key) {
        return get(key.toCharArray());
    }

    public void put(char[] keys, V value) {
        int count = 0;
        LinkedBucket<Character, TrieNode<V>> bucket = trieBucket;
        while (count < keys.length) {
            SimpleMapEntry<Character, TrieNode<V>> entry = setNode(keys[count], bucket);
            if (count == keys.length - 1) {
                entry.getValue().setNodeValue(value);
            }
            bucket = entry.getValue().getNodeBucket();
            count++;
        }
    }

    @Nullable
    V get(char[] keys) {
        int count = 0;
        LinkedBucket<Character, TrieNode<V>> bucket = trieBucket;
        while (count < keys.length) {
            SimpleMapEntry<Character, TrieNode<V>> entry = bucket.get(keys[count]);
            if (entry == null) {
                return null;
            } else {
                if (count == keys.length - 1) {
                    return entry.getValue().getNodeValue();
                } else {
                    bucket = entry.getValue().getNodeBucket();
                    count++;
                }
            }
        }
        return null;
    }

    private SimpleMapEntry<Character, TrieNode<V>> setNode(Character key, LinkedBucket<Character, TrieNode<V>> bucket) {
        SimpleMapEntry<Character, TrieNode<V>> entry = bucket.get(key);
        if (entry == null) {
            entry = new SimpleMapEntry<>(key, new TrieNode<>());
            bucket.put(entry);
        }
        return entry;
    }
}
