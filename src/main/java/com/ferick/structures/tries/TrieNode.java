package com.ferick.structures.tries;

import com.ferick.structures.maps.LinkedBucket;

class TrieNode<V> {

    private V nodeValue;
    private final LinkedBucket<Character, TrieNode<V>> nodeBucket = new LinkedBucket<>();

    V getNodeValue() {
        return nodeValue;
    }

    void setNodeValue(V nodeValue) {
        this.nodeValue = nodeValue;
    }

    LinkedBucket<Character, TrieNode<V>> getNodeBucket() {
        return nodeBucket;
    }
}
