package com.ferick.helpers

import java.nio.file.Files
import java.nio.file.Path
import java.util.stream.Collectors

private val regex = Regex("test.\\d+.(in|out)")

fun getTestFilesFromDirectory(path: Path): List<Pair<Path, Path>> {
    val testFiles: List<Path> = Files.list(path)
        .filter {
            it.fileName.toString().matches(regex)
        }
        .collect(Collectors.toList()).sorted()
    val (ins, outs) = testFiles.partition {
        it.toString().endsWith("in")
    }
    if (ins.size != outs.size) {
        throw IllegalStateException("Number of 'in' files does not equal to number of 'out' files")
    }
    val pairs = List(ins.size) { i ->
        Pair(ins[i], outs[i])
    }
    return pairs
}