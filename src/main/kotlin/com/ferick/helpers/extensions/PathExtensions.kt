package com.ferick.helpers.extensions

import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path

@ExperimentalUnsignedTypes
fun Path.readAllUBytes(): UByteArray {
    val text = Files.readString(this)
    return text.toByteArray(Charset.forName("Windows-1251")).map { it.toUByte() }.toUByteArray()
}