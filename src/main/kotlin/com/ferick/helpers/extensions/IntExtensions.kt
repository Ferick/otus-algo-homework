package com.ferick.helpers.extensions

import kotlin.math.abs

fun Int.radixCount(): Int {
    if (this == 0) {
        return 1
    }
    var radixCount = 0
    var result = abs(this)
    while (result > 0) {
        result /= 10
        radixCount++
    }
    return radixCount
}
