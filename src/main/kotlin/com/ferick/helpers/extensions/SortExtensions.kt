package com.ferick.helpers.extensions

fun IntArray.swap(left: Int, right: Int) {
    val tmp = this[left]
    this[left] = this[right]
    this[right] = tmp
}

fun IntArray.findMax(right: Int): Int {
    var max = 0
    for (i in 1..right) {
        if (this[i] > this[max]) {
            max = i
        }
    }
    return max
}

fun IntArray.heapify(root: Int, size: Int) {
    var max = root
    val left = 2 * root + 1
    val right = 2 * root + 2
    if (left < size && this[left] > this[max]) {
        max = left
    }
    if (right < size && this[right] > this[max]) {
        max = right
    }
    if (max == root) {
        return
    }
    swap(root, max)
    heapify(max, size)
}