package com.ferick.helpers.extensions

import com.ferick.structures.arrays.BasicArray
import com.ferick.structures.arrays.VectorArray

fun <T> BasicArray<T>.forEach(block: (T) -> Unit) {
    (0 until this.size()).forEach {
        val value = this.get(it)
        block.invoke(value)
    }
}

fun <T> BasicArray<T>.contains(value: T): Boolean {
    var contains = false
    this.forEach {
        if (it == value) {
            contains = true
        }
    }
    return contains
}

fun <T> BasicArray<T>.filter(block: (T) -> Boolean): VectorArray<T> {
    val array = VectorArray<T>()
    this.forEach {
        if (block.invoke(it)) {
            array.add(it)
        }
    }
    return array
}