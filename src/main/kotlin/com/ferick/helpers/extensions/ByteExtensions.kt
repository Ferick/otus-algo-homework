package com.ferick.helpers.extensions

infix fun UByte.shl(bitCount: Int): UByte = (this.toInt() shl bitCount).toUByte()

infix fun UByte.shr(bitCount: Int): UByte = (this.toInt() shr bitCount).toUByte()