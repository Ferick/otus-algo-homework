package com.ferick

interface Solver {

    fun solve(input: List<String>): String
}