package com.ferick.archiver

interface Archiver {

    fun compressFile(dataFilename: String, archiveFilename: String)

    fun decompressFile(archiveFilename: String, dataFilename: String)
}