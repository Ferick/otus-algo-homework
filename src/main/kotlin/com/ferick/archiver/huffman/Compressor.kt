package com.ferick.archiver.huffman

import com.ferick.helpers.extensions.shl

@ExperimentalUnsignedTypes
internal class Compressor {

    internal fun compressBytes(data: UByteArray): UByteArray {
        val frequency = calculateFrequency(data)
        val headers = createFileHeaders(data.size, frequency)
        val root = frequency.createTree()
        val codes = createCodes(root)
        val body = compressData(data, codes)
        return headers + body
    }

    private fun calculateFrequency(data: UByteArray): IntArray {
        val symbolFrequency = IntArray(256) { 0 }
        data.forEach {
            symbolFrequency[it.toInt()]++
        }
        return symbolFrequency
    }

    private fun createFileHeaders(dataLength: Int, frequency: IntArray): UByteArray {
        val fileHeaders = mutableListOf<UByte>()

        fileHeaders.add((dataLength and 255).toUByte())
        fileHeaders.add(((dataLength shr 8) and 255).toUByte())
        fileHeaders.add(((dataLength shr 16) and 255).toUByte())
        fileHeaders.add(((dataLength shr 24) and 255).toUByte())

        val symbolCount = frequency.count { it > 0 }
        fileHeaders.add(symbolCount.toUByte())

        frequency.asSequence().forEachIndexed { symbol, frequentCount ->
            if (frequentCount > 0) {
                fileHeaders.add(symbol.toUByte())
                fileHeaders.add(frequentCount.toUByte())
            }
        }

        return fileHeaders.toUByteArray()
    }

    private fun createCodes(root: Node): Array<String> {
        val codes = Array(256) { "" }

        fun next(node: Node, code: String) {
            if (node.symbol != null) {
                codes[node.symbol.toInt()] = code
            } else {
                next(node.bit0!!, "${code}0")
                next(node.bit1!!, "${code}1")
            }
        }

        next(root, "")
        return codes
    }

    private fun compressData(data: UByteArray, codes: Array<String>): UByteArray {
        val result = mutableListOf<UByte>()
        val completedByte: UByte = 128u
        var sum: UByte = 0u
        var bit: UByte = 1u
        data.forEach { symbol ->
            codes[symbol.toInt()].forEach { char ->
                if (char == '1') {
                    sum = sum or bit
                }
                if (bit == completedByte) {
                    result.add(sum)
                    sum = 0u
                    bit = 1u
                } else {
                    bit = bit shl 1
                }
            }
        }
        if (bit > 1u) {
            result.add(sum)
        }
        return result.toUByteArray()
    }
}
