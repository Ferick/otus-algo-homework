package com.ferick.archiver.huffman

data class HeaderInfo(
    val dataLength: Int,
    val symbolFrequency: IntArray,
    val startIndex: Int
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as HeaderInfo

        if (dataLength != other.dataLength) return false
        if (!symbolFrequency.contentEquals(other.symbolFrequency)) return false
        if (startIndex != other.startIndex) return false

        return true
    }

    override fun hashCode(): Int {
        var result = dataLength
        result = 31 * result + symbolFrequency.contentHashCode()
        result = 31 * result + startIndex
        return result
    }
}
