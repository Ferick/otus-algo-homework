package com.ferick.archiver.huffman

internal data class Node(
    val frequency: Int,
    val bit0: Node? = null,
    val bit1: Node? = null,
    val symbol: UByte? = null
) {
    internal constructor(bit0: Node, bit1: Node) : this(bit0.frequency + bit1.frequency, bit0, bit1)
}
