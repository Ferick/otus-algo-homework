package com.ferick.archiver.huffman

@ExperimentalUnsignedTypes
internal class Decompressor {

    internal fun decompressBytes(archive: UByteArray): UByteArray {
        val headerInfo = parseFileHeaders(archive)
        val root = headerInfo.symbolFrequency.createTree()
        return decompressData(archive, headerInfo.startIndex, headerInfo.dataLength, root)
    }

    private fun parseFileHeaders(archive: UByteArray): HeaderInfo {
        val dataLength = archive[0].toInt() or
                (archive[1].toInt() shl 8) or
                (archive[2].toInt() shl 16) or
                (archive[3].toInt() shl 24)

        val symbolFrequency = IntArray(256) { 0 }
        val count = archive[4].toInt()
        (0 until count).forEach {
            val symbol = archive[5 + it * 2].toInt()
            symbolFrequency[symbol] = archive[6 + it * 2].toInt()
        }

        val startIndex = 4 + 1 + 2 * count

        return HeaderInfo(dataLength, symbolFrequency, startIndex)
    }

    private fun decompressData(archive: UByteArray, startIndex: Int, dataLength: Int, root: Node): UByteArray {
        var size = 0
        var currentNode = root
        val data = UByteArray(dataLength)
        (startIndex until archive.size).forEach { index ->
            var bit = 1
            while (bit <= 128) {
                currentNode = if ((archive[index].toInt() and bit) == 0) {
                    currentNode.bit0!!
                } else {
                    currentNode.bit1!!
                }
                if (currentNode.symbol != null) {
                    if (size < dataLength) {
                        data[size++] = currentNode.symbol!!
                    }
                    currentNode = root
                }
                bit = bit shl 1
            }
        }
        return data
    }
}
