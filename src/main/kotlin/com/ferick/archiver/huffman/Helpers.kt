package com.ferick.archiver.huffman

internal fun IntArray.createTree(): Node {
    val queue = FrequentPriorityQueue<Node>()
    this.asSequence().forEachIndexed { symbol, frequentCount ->
        if (frequentCount > 0) {
            queue.enqueue(frequentCount, Node(frequency = frequentCount, symbol = symbol.toUByte()))
        }
    }
    while (queue.size > 1) {
        val bit0 = queue.dequeue()
        val bit1 = queue.dequeue()
        val parent = Node(bit0!!, bit1!!)
        queue.enqueue(parent.frequency, parent)
    }
    return queue.dequeue()!!
}