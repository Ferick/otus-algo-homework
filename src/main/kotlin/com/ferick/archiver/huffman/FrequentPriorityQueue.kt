package com.ferick.archiver.huffman

import com.ferick.structures.queues.SimpleQueue
import java.util.*

internal class FrequentPriorityQueue<T> {

    private val storage = TreeMap<Int, SimpleQueue<T>>()
    var size = 0

    internal fun enqueue(priority: Int, item: T) {
        if (!storage.containsKey(priority)) {
            storage[priority] = SimpleQueue()
        }
        storage[priority]!!.put(item)
        size++
    }

    internal fun dequeue(): T? {
        storage.values.forEach { queue ->
            if (!queue.isEmpty) {
                size--
                return queue.take()
            }
        }
        return null
    }

    internal fun isEmpty(): Boolean = size == 0
}
