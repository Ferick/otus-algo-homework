package com.ferick.archiver.huffman

import com.ferick.archiver.Archiver
import com.ferick.helpers.extensions.readAllUBytes
import com.ferick.helpers.extensions.toPath
import java.nio.file.Files

@ExperimentalUnsignedTypes
class HuffmanArchiver : Archiver {

    private val compressor = Compressor()
    private val decompressor = Decompressor()

    override fun compressFile(dataFilename: String, archiveFilename: String) {
        val data = dataFilename.toPath().readAllUBytes()
        val archive = compressor.compressBytes(data)
        Files.write(archiveFilename.toPath(), archive.toByteArray())
    }

    override fun decompressFile(archiveFilename: String, dataFilename: String) {
        val archive = Files.readAllBytes(archiveFilename.toPath())
        val data = decompressor.decompressBytes(archive.toUByteArray())
        Files.write(dataFilename.toPath(), data.toByteArray())
    }
}
