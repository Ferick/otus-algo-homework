package com.ferick.model

import java.math.BigInteger

data class Matrix(
    val leftUpper: BigInteger,
    val rightUpper: BigInteger,
    val leftLower: BigInteger,
    val rightLower: BigInteger
) {

    fun square(): Matrix = this.multiply(this)

    fun multiply(other: Matrix): Matrix {
        val leftUpperResult = operate(this.leftUpper, other.leftUpper, this.rightUpper, other.leftLower)
        val rightUpperResult = operate(this.leftUpper, other.rightUpper, this.rightUpper, other.rightLower)
        val leftLowerResult = operate(this.leftLower, other.leftUpper, this.rightLower, other.leftLower)
        val rightLowerResult = operate(this.leftLower, other.rightUpper, this.rightLower, other.rightLower)
        return Matrix(leftUpperResult, rightUpperResult, leftLowerResult, rightLowerResult)
    }

    private fun operate(
        leftFirst: BigInteger,
        leftSecond: BigInteger,
        rightFirst: BigInteger,
        rightSecond: BigInteger
    ): BigInteger {
        return leftFirst * leftSecond + rightFirst * rightSecond
    }
}
