package com.ferick.annotations

import com.ferick.Solver
import kotlin.reflect.KClass

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class SolverType(val value: KClass<out Solver>, vararg val values: KClass<out Solver>)
