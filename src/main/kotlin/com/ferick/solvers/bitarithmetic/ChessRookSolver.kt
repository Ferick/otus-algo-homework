package com.ferick.solvers.bitarithmetic

import com.ferick.Solver

class ChessRookSolver : Solver {

    override fun solve(input: List<String>): String {
        val position = input[0].toInt()
        val mask = ChessSolverFunctions.getRookMoves(position)
        return "${BitCounter.count(mask)},$mask"
    }
}
