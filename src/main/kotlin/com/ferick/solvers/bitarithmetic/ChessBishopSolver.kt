package com.ferick.solvers.bitarithmetic

import com.ferick.Solver

class ChessBishopSolver : Solver {

    override fun solve(input: List<String>): String {
        val position = input[0].toInt()
        val mask = ChessSolverFunctions.getBishopMoves(position)
        return "${BitCounter.count(mask)},$mask"
    }
}
