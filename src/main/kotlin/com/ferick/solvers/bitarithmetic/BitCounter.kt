package com.ferick.solvers.bitarithmetic

object BitCounter {

    private val bits = (0 until 256).map { counter(it.toULong()) }.toTypedArray()

    fun count(mask: ULong): Int {
        var count = 0
        var result = mask
        while (result > 0UL) {
            val index = (result and 255UL).toInt()
            count += bits[index]
            result = result shr 8
        }
        return count
    }

    private fun counter(mask: ULong): Int {
        var count = 0
        var result = mask
        while (result > 0UL) {
            result = result and (result - 1UL)
            count++
        }
        return count
    }
}
