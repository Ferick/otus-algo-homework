package com.ferick.solvers.bitarithmetic

import com.ferick.Solver

class ChessKnightSolver : Solver {

    override fun solve(input: List<String>): String {
        val position = input[0].toInt()
        val mask = getKnightMoves(position)
        return "${BitCounter.count(mask)},$mask"
    }

    private fun getKnightMoves(position: Int): ULong {
        val knight = 1UL shl position
        val knightLeft = knight and NO_LEFT
        val knightLeftDouble = knight and NO_LEFT_DOUBLE
        val knightRight = knight and NO_RIGHT
        val knightRightDouble = knight and NO_RIGHT_DOUBLE
        return (knightLeftDouble shl 6) or (knightRight shl 10) or (knightLeft shl 15) or (knightRight shl 17) or
               (knightRightDouble shr 6) or (knightLeft shr 10) or (knightRight shr 15) or (knightLeft shr 17)
    }
}