package com.ferick.solvers.bitarithmetic

import com.ferick.Solver

class ChessKingSolver : Solver {

    override fun solve(input: List<String>): String {
        val position = input[0].toInt()
        val mask = getKingMoves(position)
        return "${BitCounter.count(mask)},$mask"
    }

    private fun getKingMoves(position: Int): ULong {
        val king = 1UL shl position
        val kingLeft = king and NO_LEFT
        val kingRight = king and NO_RIGHT
        return (kingLeft shl 7) or (king shl 8) or (kingRight shl 9) or
               (kingLeft shr 1)                 or (kingRight shl 1) or
               (kingLeft shr 9) or (king shr 8) or (kingRight shr 7)
    }
}