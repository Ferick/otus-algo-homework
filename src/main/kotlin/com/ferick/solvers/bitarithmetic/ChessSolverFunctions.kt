package com.ferick.solvers.bitarithmetic

object ChessSolverFunctions {

    fun getRookMoves(position: Int): ULong {
        return (LEFT_LINE shl (position and 7)) xor (BOTTOM_LINE shl ((position shr 3) shl 3))
    }

    fun getBishopMoves(position: Int): ULong {
        val pos = 1UL shl position
        return recursiveUpperLeft(pos) or recursiveUpperRight(pos) or
                recursiveLowerLeft(pos) or recursiveLowerRight(pos)
    }

    private fun recursiveUpperLeft(position: ULong): ULong {
        if (position and NO_LEFT == 0UL) {
            return 0UL
        }
        val next = position shl 7
        return next or recursiveUpperLeft(next)
    }

    private fun recursiveUpperRight(position: ULong): ULong {
        if (position and NO_RIGHT == 0UL) {
            return 0UL
        }
        val next = position shl 9
        return next or recursiveUpperRight(next)
    }

    private fun recursiveLowerLeft(position: ULong): ULong {
        if (position and NO_LEFT == 0UL) {
            return 0UL
        }
        val next = position shr 9
        return next or recursiveLowerLeft(next)
    }

    private fun recursiveLowerRight(position: ULong): ULong {
        if (position and NO_RIGHT == 0UL) {
            return 0UL
        }
        val next = position shr 7
        return next or recursiveLowerRight(next)
    }
}