package com.ferick.solvers.tickets

import com.ferick.Solver

class TicketSolver : Solver {

    override fun solve(input: List<String>) = solveRecursively(input)

    private fun solveRecursively(input: List<String>): String {
        var count = 0

        fun recursiveFunction(remainingDigits: Int, leftSum: Int, rightSum: Int) {
            if (remainingDigits == 0) {
                if (leftSum == rightSum) {
                    count++
                }
                return
            }
            for (a in 0..9) {
                for (b in 0..9) {
                    recursiveFunction(remainingDigits - 1, leftSum + a, rightSum + b)
                }
            }
        }

        recursiveFunction(input[0].toInt(), 0, 0)

        return count.toString()
    }
}