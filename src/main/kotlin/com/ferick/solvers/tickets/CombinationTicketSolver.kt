package com.ferick.solvers.tickets

import com.ferick.Solver

class CombinationTicketSolver : Solver {

    override fun solve(input: List<String>): String {
        val remainingDigits = input[0].toInt()
        val maxSum = remainingDigits * 9
        var result = 0L
        for (sum in 0..maxSum) {
            val combinations = solveRecursively(remainingDigits, sum, maxSum)
            result += combinations * combinations
        }
        return result.toString()
    }

    private fun solveRecursively(remainingDigits: Int, sum: Int, maxSum: Int): Long {
        if (remainingDigits == 1) {
            for (i in 0..9) {
                if (sum + i == maxSum) {
                    return 1
                }
            }
            return 0
        }
        var result = 0L
        for (i in 0..9) {
            result += solveRecursively(remainingDigits - 1, sum + i, maxSum)
        }
        return result
    }
}