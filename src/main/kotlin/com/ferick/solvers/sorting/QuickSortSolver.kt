package com.ferick.solvers.sorting

import com.ferick.Solver
import com.ferick.helpers.extensions.swap

class QuickSortSolver : Solver {

    override fun solve(input: List<String>): String {
        val array = input[0].split(" ").map { it.toInt() }.toIntArray()
        return sort(array).reduce { acc, s -> "$acc $s" }
    }

    private fun sort(array: IntArray): List<String> {
        quickSort(array, 0, array.size - 1)
        return array.map { it.toString() }
    }

    private fun quickSort(array: IntArray, left: Int, right: Int) {
        if (left >= right) {
            return
        }
        val middle = split(array, left, right)
        quickSort(array, left, middle - 1)
        quickSort(array, middle + 1, right)
    }

    private fun split(array: IntArray, left: Int, right: Int): Int {
        val prop = array[right]
        var middle = left - 1
        for (i in left..right) {
            if (array[i] <= prop) {
                array.swap(++middle, i)
            }
        }
        return middle
    }
}
