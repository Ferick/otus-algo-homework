package com.ferick.solvers.sorting

import com.ferick.Solver

class MergeSortSolver : Solver {

    override fun solve(input: List<String>): String {
        val array = input[0].split(" ").map { it.toInt() }.toIntArray()
        return sort(array).reduce { acc, s -> "$acc $s" }
    }

    private fun sort(array: IntArray): List<String> {
        mergeSort(array, 0, array.size - 1)
        return array.map { it.toString() }
    }

    private fun mergeSort(array: IntArray, left: Int, right: Int) {
        if (left >= right) {
            return
        }
        val middle = (left + right) / 2
        mergeSort(array, left, middle)
        mergeSort(array, middle + 1, right)
        merge(array, left, middle, right)
    }

    private fun merge(array: IntArray, left: Int, middle: Int, right: Int) {
        val tmpArray = IntArray(right - left + 1) { 0 }
        var a = left
        var b = middle + 1
        var tmp = 0
        while (a <= middle && b <= right) {
            if (array[a] < array[b]) {
                tmpArray[tmp++] = array[a++]
            } else {
                tmpArray[tmp++] = array[b++]
            }
        }
        while (a <= middle) {
            tmpArray[tmp++] = array[a++]
        }
        while (b <= right) {
            tmpArray[tmp++] = array[b++]
        }
        for (i in left..right) {
            array[i] = tmpArray[i - left]
        }
    }
}
