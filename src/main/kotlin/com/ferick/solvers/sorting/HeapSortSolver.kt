package com.ferick.solvers.sorting

import com.ferick.Solver
import com.ferick.helpers.extensions.heapify
import com.ferick.helpers.extensions.swap

class HeapSortSolver : Solver {

    override fun solve(input: List<String>): String {
        val array = input[0].split(" ").map { it.toInt() }.toIntArray()
        return sort(array).reduce { acc, s -> "$acc $s" }
    }

    private fun sort(array: IntArray): List<String> {
        for (j in (array.size / 2 - 1) downTo 0) {
            array.heapify(j, array.size)
        }
        for (j in (array.size - 1) downTo 1) {
            array.swap(0, j)
            array.heapify(0, j)
        }
        return array.map { it.toString() }
    }
}
