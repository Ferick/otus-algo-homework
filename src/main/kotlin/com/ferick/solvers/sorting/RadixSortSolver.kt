package com.ferick.solvers.sorting

import com.ferick.Solver
import com.ferick.helpers.extensions.findMax
import com.ferick.helpers.extensions.radixCount

class RadixSortSolver : Solver {

    override fun solve(input: List<String>): String {
        val array = input[0].split(" ").map { it.toInt() }.toIntArray()
        return sort(array).reduce { acc, s -> "$acc $s" }
    }

    fun sort(array: IntArray): List<String> {
        val maxValue = array[array.findMax(array.size - 1)]
        val radixCount = maxValue.radixCount()
        return counting(array, 1, radixCount).map { it.toString() }
    }

    private fun counting(array: IntArray, radix: Int, radixCount: Int): IntArray {
        if (radix > radixCount) {
            return array
        }
        val sumArray = getSumArray(array, radix)
        val sortedArray = IntArray(array.size) { 0 }
        for (index in (array.size - 1) downTo 0) {
            val value = array[index]
            val number = getNumberByRadix(value, radix)
            val sortedIndex = --sumArray[number]
            sortedArray[sortedIndex] = value
        }
        return counting(sortedArray, radix + 1, radixCount)
    }

    private fun getSumArray(array: IntArray, radix: Int): IntArray {
        val sumArray = IntArray(10) { 0 }
        array.forEach {
            val number = getNumberByRadix(it, radix)
            sumArray[number]++
        }
        for (i in 1 until sumArray.size) {
            sumArray[i] = sumArray[i - 1] + sumArray[i]
        }
        return sumArray
    }

    private fun getNumberByRadix(number: Int, radix: Int): Int {
        if (radix == 1) {
            return number % 10
        }
        val result = number / 10
        if (result == 0) {
            return 0
        }
        return getNumberByRadix(number / 10, radix - 1)
    }
}
