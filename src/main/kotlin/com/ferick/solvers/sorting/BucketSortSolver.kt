package com.ferick.solvers.sorting

import com.ferick.Solver
import com.ferick.helpers.extensions.findMax
import com.ferick.structures.queues.sorted.SortedQueue

class BucketSortSolver : Solver {

    override fun solve(input: List<String>): String {
        val array = input[0].split(" ").map { it.toInt() }.toIntArray()
        return sort(array).reduce { acc, s -> "$acc $s" }
    }

    fun sort(array: IntArray): List<String> {
        var maxValue = array[array.findMax(array.size - 1)]
        maxValue++
        return bucketSort(array, maxValue).map { it.toString() }
    }

    private fun bucketSort(array: IntArray, maxValue: Int): IntArray {
        val buckets = Array<SortedQueue<Int>?>(array.size) { null }
        array.forEach {
            val bucketIndex = (it * array.size) / maxValue
            if (buckets[bucketIndex] == null) {
                buckets[bucketIndex] = SortedQueue()
            }
            buckets[bucketIndex]!!.put(it)
        }
        val sortedArray = IntArray(array.size) { 0 }
        var arrayIndex = 0
        buckets.forEach {
            it?.let {
                while (!it.isEmpty) {
                    sortedArray[arrayIndex++] = it.take()
                }
            }
        }
        return sortedArray
    }
}
