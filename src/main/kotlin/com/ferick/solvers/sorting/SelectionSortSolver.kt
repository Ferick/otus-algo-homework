package com.ferick.solvers.sorting

import com.ferick.Solver
import com.ferick.helpers.extensions.findMax
import com.ferick.helpers.extensions.swap

class SelectionSortSolver : Solver {

    override fun solve(input: List<String>): String {
        val array = input[0].split(" ").map { it.toInt() }.toIntArray()
        return sort(array).reduce { acc, s -> "$acc $s" }
    }

    private fun sort(array: IntArray): List<String> {
        for (j in (array.size - 1) downTo 1) {
            array.swap(array.findMax(j), j)
        }
        return array.map { it.toString() }
    }
}
