package com.ferick.solvers.sorting

import com.ferick.Solver
import com.ferick.helpers.extensions.swap

class InsertionSortSolver : Solver {

    override fun solve(input: List<String>): String {
        val array = input[0].split(" ").map { it.toInt() }.toIntArray()
        return sort(array).reduce { acc, s -> "$acc $s" }
    }

    private fun sort(array: IntArray): List<String> {
        for (j in array.indices) {
            for (i in (j - 1) downTo 0) {
                if (array[i] > array[i + 1]) {
                    array.swap(i, i + 1)
                } else {
                    break
                }
            }
        }
        return array.map { it.toString() }
    }
}
