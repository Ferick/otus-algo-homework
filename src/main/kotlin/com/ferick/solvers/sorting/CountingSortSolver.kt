package com.ferick.solvers.sorting

import com.ferick.Solver
import com.ferick.helpers.extensions.findMax

class CountingSortSolver : Solver {

    override fun solve(input: List<String>): String {
        val array = input[0].split(" ").map { it.toInt() }.toIntArray()
        return sort(array).reduce { acc, s -> "$acc $s" }
    }

    private fun sort(array: IntArray): List<String> {
        val maxValue = array[array.findMax(array.size - 1)]
        return countingSort(array, maxValue).map { it.toString() }
    }

    private fun countingSort(array: IntArray, maxValue: Int): IntArray {
        val numCounts = IntArray(maxValue + 1) { 0 }
        array.forEach {
            numCounts[it]++
        }
        val sortedArray = IntArray(array.size) { 0 }
        var currentSortedIndex = 0
        for (value in numCounts.indices) {
            val count = numCounts[value]
            for (i in 0 until count) {
                sortedArray[currentSortedIndex] = value
                currentSortedIndex++
            }
        }
        return sortedArray
    }
}
