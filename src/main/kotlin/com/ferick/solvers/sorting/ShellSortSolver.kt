package com.ferick.solvers.sorting

import com.ferick.Solver
import com.ferick.helpers.extensions.swap

class ShellSortSolver : Solver {

    override fun solve(input: List<String>): String {
        val array = input[0].split(" ").map { it.toInt() }.toIntArray()
        return sort(array).reduce { acc, s -> "$acc $s" }
    }

    private fun sort(array: IntArray): List<String> {
        var gap = array.size / 2
        while (gap > 0) {
            for (j in gap until array.size) {
                var i = j
                while (i >= gap && array[i - gap] > array[i]) {
                    array.swap(i - gap, i)
                    i -= gap
                }
            }
            gap /= 2
        }
        return array.map { it.toString() }
    }
}

