package com.ferick.solvers.algebraic.primes

import com.ferick.Solver
import kotlin.math.sqrt

class PrimesSolver : Solver {

    override fun solve(input: List<String>): String {
        val number = input[0].toLong()
        return primesCount(number).toString()
    }

    private fun primesCount(number: Long): Long {
        if (number == 1L) {
            return 0
        }

        var count = 0L
        val primes = mutableListOf<Long>()
        primes.add(2)
        count++

        for (i in 3..number) {
            if (isPrime(primes, i)) {
                primes.add(i)
                count++
            }
        }
        return count
    }

    private fun isPrime(primes: List<Long>, number: Long): Boolean {
        val sqrt = sqrt(number.toDouble())
        primes.takeWhile { it <= sqrt }.forEach {
            if (number % it == 0L) {
                return false
            }
        }
        return true
    }
}
