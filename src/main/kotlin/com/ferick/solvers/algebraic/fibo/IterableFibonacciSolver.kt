package com.ferick.solvers.algebraic.fibo

import com.ferick.Solver
import java.math.BigInteger

class IterableFibonacciSolver : Solver {

    override fun solve(input: List<String>): String {
        val number = input[0].toLong()
        return iterate(number).toString()
    }

    private fun iterate(number: Long): BigInteger {
        var a = BigInteger("0")
        var b = BigInteger("1")

        if (number == 0L) {
            return a
        } else if (number == 1L) {
            return b
        }

        for (i in 2..number) {
            val next = a + b
            a = b
            b = next
        }

        return b
    }
}
