package com.ferick.solvers.algebraic.fibo

import com.ferick.Solver
import com.ferick.model.Matrix

class MatrixFibonacciSolver : Solver {

    override fun solve(input: List<String>): String {
        val number = input[0].toLong()
        val base = Matrix(1.toBigInteger(), 1.toBigInteger(), 1.toBigInteger(), 0.toBigInteger())
        if (number == 0L) {
            return "0"
        }
        return solveRecursively(base, number).rightUpper.toString()
    }

    private fun solveRecursively(base: Matrix, power: Long): Matrix {
        if (power == 1L) {
            return base
        }
        val divisible = power / 2
        val remainder = power % 2
        val result = solveRecursively(base, divisible).square()
        return if (remainder == 1L) result.multiply(base) else result
    }
}