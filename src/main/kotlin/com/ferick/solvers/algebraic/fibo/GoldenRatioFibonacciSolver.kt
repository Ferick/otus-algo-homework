package com.ferick.solvers.algebraic.fibo

import com.ferick.Solver
import kotlin.math.pow
import kotlin.math.sqrt

class GoldenRatioFibonacciSolver : Solver {

    override fun solve(input: List<String>): String {
        val number = input[0].toInt()
        return findByGoldenRatio(number).toString()
    }

    private fun findByGoldenRatio(number: Int): Long {
        val sqrt = sqrt(5.0)
        val goldenRatio = (sqrt + 1) / 2
        return (goldenRatio.pow(number) / sqrt + 0.5).toLong()
    }
}