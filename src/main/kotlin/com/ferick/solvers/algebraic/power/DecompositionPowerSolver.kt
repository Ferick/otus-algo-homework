package com.ferick.solvers.algebraic.power

import com.ferick.Solver

class DecompositionPowerSolver : Solver {

    override fun solve(input: List<String>): String {
        val base = input[0].toDouble()
        val power = input[1].toLong()
        if (power == 0L) {
            return 1.0.toString()
        } else if (power == 1L) {
            return base.toString()
        }
        return decompose(base, power).toString()
    }

    private fun decompose(base: Double, power: Long): Double {
        var powerNumber = power
        var decomposable = base
        var result = 1.0
        while (powerNumber >= 1) {
            powerNumber /= 2
            decomposable *= decomposable
            if (powerNumber % 2 != 0L) {
                result *= decomposable
            }
        }
        return result
    }
}
