package com.ferick.solvers.algebraic.power

import com.ferick.Solver

class IterablePowerSolver : Solver {

    override fun solve(input: List<String>): String {
        val base = input[0].toDouble()
        val power = input[1].toLong()
        if (power == 0L) {
            return 1.0.toString()
        } else if (power == 1L) {
            return base.toString()
        }
        var result = base
        for (i in 2..power) {
            result *= base
        }
        return result.toString()
    }
}
