package com.ferick.solvers.algebraic.power

import com.ferick.Solver

class BinaryMultiplyPowerSolver : Solver {

    override fun solve(input: List<String>): String {
        val base = input[0].toDouble()
        val power = input[1].toLong()
        if (power == 0L) {
            return 1.0.toString()
        } else if (power == 1L) {
            return base.toString()
        }
        val result = solveRecursively(base, power)
        return result.toString()
    }

    private fun solveRecursively(base: Double, power: Long): Double {
        if (power == 1L) {
            return base
        }
        val divisible = power / 2
        val remainder = power % 2
        val result = solveRecursively(base, divisible) * solveRecursively(base, divisible)
        return if (remainder == 1L) result * base else result
    }
}
