package com.ferick.graphs

import com.ferick.structures.arrays.VectorArray
import com.ferick.structures.graphs.AdjacencyVector
import com.ferick.structures.graphs.VectorItem

/**
 * Graph topology search using Demukron's algorithm
 */
class AnotherTopologySearch(private val graph: AdjacencyVector) {

    private val inDegree = loadInDegree(graph)
    private val finalVertexList = mutableListOf<Int>()

    fun search(): List<Int> {
        while (inDegree.greaterThanZero()) {
            inDegree.checkZeroLevels()
        }
        inDegree.checkLastZero()
        return finalVertexList
    }

    private fun loadInDegree(graph: AdjacencyVector): MutableMap<Int, Int> {
        val map = (0 until graph.size()).associateWith { 0 }.toMutableMap()
        (0 until graph.size()).forEach { vertexIndex ->
            val vectors = graph.getVectors(vertexIndex)
            (0 until vectors.size()).forEach {
                val vectorItem = vectors.get(it)
                var value = map[vectorItem.vertexIndex]!!
                map[vectorItem.vertexIndex] = ++value
            }
        }
        return map
    }

    private fun MutableMap<Int, Int>.greaterThanZero(): Boolean {
        return this.values.any { it > 0 }
    }

    private fun MutableMap<Int, Int>.checkLastZero() {
        this.filterValues { it == 0 }.forEach { (vertexIndex, _) -> vertexIndex.erase() }
    }

    private fun MutableMap<Int, Int>.checkZeroLevels() {
        this.filterValues { it == 0 }.forEach { (vertexIndex, _) ->
            val vectors = graph.getVectors(vertexIndex)
            if (!vectors.isEmpty) {
                this.minusLevel(vertexIndex, vectors)
            }
        }
    }

    private fun MutableMap<Int, Int>.minusLevel(vertexIndex: Int, vectors: VectorArray<VectorItem>) {
        vertexIndex.erase()
        (0 until vectors.size()).forEach {
            val vectorItem = vectors.get(it)
            var value = this[vectorItem.vertexIndex]!!
            if (value >= 0) {
                this[vectorItem.vertexIndex] = --value
            }
        }
    }

    private fun Int.erase() {
        val value = inDegree[this]
        if (value != 0) {
            throw IllegalStateException("Value to be erased must be 0")
        }
        inDegree[this] = -1
        finalVertexList.add(this)
    }
}