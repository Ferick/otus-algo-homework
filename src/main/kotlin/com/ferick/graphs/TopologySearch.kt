package com.ferick.graphs

import com.ferick.structures.graphs.AdjacencyVector

/**
 * Graph topology search using Tarjan's algorithm
 */
class TopologySearch(private val graph: AdjacencyVector) {

    private val vertices = (0 until graph.size()).associateWith { VertexColor.WHITE }.toMutableMap()
    private val finalVertexList = mutableListOf<Int>()

    fun search(index: Int): List<Int> {
        collectVertices(index)
        index.finalizeColor()
        return finalVertexList
    }

    private fun collectVertices(index: Int) {
        if (index.color() != VertexColor.WHITE) {
            return
        }
        index.makeGrey()
        val vectors = graph.getVectors(index)
        val whiteItems = (0 until vectors.size())
            .map { vectors.get(it) }
            .filter { it.vertexIndex.color() == VertexColor.WHITE }
        if (whiteItems.isEmpty()) {
            index.finalizeColor()
        } else {
            whiteItems.forEach {
                collectVertices(it.vertexIndex)
                it.vertexIndex.finalizeColor()
            }
        }
    }

    private fun Int.color(): VertexColor {
        return vertices[this] ?: throw IllegalStateException("Vertex $this is missing")
    }

    private fun Int.finalizeColor() {
        if (this.color() == VertexColor.GREY) {
            this.makeBlack()
            finalVertexList.add(this)
        }
    }

    private fun Int.makeGrey() {
        vertices[this] = VertexColor.GREY
    }

    private fun Int.makeBlack() {
        vertices[this] = VertexColor.BLACK
    }
}