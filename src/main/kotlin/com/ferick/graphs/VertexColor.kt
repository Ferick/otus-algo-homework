package com.ferick.graphs

enum class VertexColor {
    WHITE, GREY, BLACK
}