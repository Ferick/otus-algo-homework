package com.ferick.graphs

import com.ferick.helpers.extensions.contains
import com.ferick.helpers.extensions.forEach
import com.ferick.structures.arrays.VectorArray
import com.ferick.structures.graphs.AdjacencyVector

/**
 * Graph the shortest path search using Dijkstra's algorithm
 */
class ShortestPathSearch(
    private val graph: AdjacencyVector
) {

    private val vertices = (0 until graph.size()).associateWith { Int.MAX_VALUE }.toMutableMap()
    private val erasedList = VectorArray<Int>()

    fun getVertexCosts(firstVertex: Int): Map<Int, Int> {
        vertices[firstVertex] = 0
        while (erasedList.size() < vertices.size) {
            vertices.minVertex().evaluateAdjacentVertices()
        }
        return vertices
    }

    private fun Map<Int, Int>.minVertex(): Int {
        val minValue = this.filterKeys { !it.isErased() }.minOf { it.value }
        return this.filter { !it.key.isErased() && it.value == minValue }.keys.first()
    }

    private fun Int.evaluateAdjacentVertices() {
        val vectors = graph.getVectors(this)
        vectors.forEach {
            if (!it.vertexIndex.isErased() && vertices[it.vertexIndex]!! > (vertices[this]!! + it.weight)) {
                vertices[it.vertexIndex] = vertices[this]!! + it.weight
            }
        }
        this.erased()
    }

    private fun Int.erased() {
        erasedList.add(this)
    }

    private fun Int.isErased(): Boolean {
        return erasedList.contains(this)
    }
}
