package com.ferick.graphs

import com.ferick.structures.graphs.AdjacencyVector

/**
 * Graph strongly connected component search using Kosaraju's algorithm
 */
class StronglyConnectedComponentSearch(
    private val graph: AdjacencyVector,
    vertexList: List<Int>
) {

    private val vertices = vertexList.reversed().associateWith { VertexColor.WHITE }.toMutableMap()
    private val components = mutableListOf<List<Int>>()

    fun search(): List<List<Int>> {
        var whiteVertices = vertices.withColor(VertexColor.WHITE)
        while (whiteVertices.isNotEmpty()) {
            val firstVertex = whiteVertices.toList()[0].first
            collectVertices(firstVertex)
            val greyVertices = vertices.withColor(VertexColor.GREY)
            val component = mutableListOf<Int>()
            greyVertices.forEach { (vertex, _) ->
                component.add(vertex)
                vertex.makeBlack()
            }
            components.add(component)
            whiteVertices = vertices.withColor(VertexColor.WHITE)
        }
        return components
    }

    private fun collectVertices(index: Int) {
        if (index.color() != VertexColor.WHITE) {
            return
        }
        index.makeGrey()
        val vectors = graph.getVectors(index)
        val whiteItems = (0 until vectors.size())
            .map { vectors.get(it) }
            .filter { it.vertexIndex.color() == VertexColor.WHITE }
        if (whiteItems.isEmpty()) {
            return
        } else {
            whiteItems.forEach {
                collectVertices(it.vertexIndex)
            }
        }
    }

    private fun Map<Int, VertexColor>.withColor(color: VertexColor): Map<Int, VertexColor> {
        return this.filterValues { it == color }
    }

    private fun Int.color(): VertexColor {
        return vertices[this] ?: throw IllegalStateException("Vertex $this is missing")
    }

    private fun Int.makeGrey() {
        vertices[this] = VertexColor.GREY
    }

    private fun Int.makeBlack() {
        vertices[this] = VertexColor.BLACK
    }
}