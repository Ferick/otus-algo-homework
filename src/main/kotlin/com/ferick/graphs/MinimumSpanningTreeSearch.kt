package com.ferick.graphs

import com.ferick.structures.arrays.VectorArray
import com.ferick.structures.graphs.AdjacencyVector
import com.ferick.structures.graphs.Edge

/**
 * Graph minimum spanning tree search using Kruskal's algorithm
 */
class MinimumSpanningTreeSearch(
    graph: AdjacencyVector,
    edges: VectorArray<Edge>
) {

    private val sortedEdges = edges.sort()
    private val vertexSet = IntArray(graph.size()) { it }
    private val minimumSpanningTree = VectorArray<Edge>()

    fun getMinimumSpanningTree(): VectorArray<Edge> {
        (0 until sortedEdges.size()).forEach { index ->
            val edge = sortedEdges.get(index)
            if (!edge.vertexOne.sameSet(edge.vertexTwo)) {
                minimumSpanningTree.add(edge)
                edge.vertexOne.union(edge.vertexTwo)
            }
        }
        return minimumSpanningTree
    }

    private fun VectorArray<Edge>.sort(): VectorArray<Edge> {
        val newArray = VectorArray<Edge>()
        while (!this.isEmpty) {
            var minWeightedIndex = 0
            (1 until this.size()).forEach {
                val current = this.get(it)
                val minWeighted = this.get(minWeightedIndex)
                if (current.weight < minWeighted.weight) {
                    minWeightedIndex = it
                }
            }
            newArray.add(this.get(minWeightedIndex))
            this.remove(minWeightedIndex)
        }
        return newArray
    }

    private fun Int.sameSet(otherVertex: Int): Boolean = this.find() == otherVertex.find()

    private fun Int.union(otherVertex: Int) {
        val rootOne = this.find()
        val rootTwo = otherVertex.find()
        if (rootOne != rootTwo) {
            vertexSet[rootOne] = rootTwo
        }
    }

    private fun Int.find(): Int {
        if (this == vertexSet[this]) {
            return this
        }
        return vertexSet[this].find()
    }
}