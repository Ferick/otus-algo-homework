package com.ferick.strings

class SubstringSearch {

    fun findIndices(text: String, mask: String): List<Int> {
        val indices = mutableListOf<Int>()
        var textIndex = 0
        while (textIndex <= text.length - mask.length) {
            var maskIndex = 0
            while (maskIndex < mask.length && text[textIndex + maskIndex] == mask[maskIndex]) {
                maskIndex++
            }
            if (maskIndex == mask.length) {
                indices.add(textIndex)
            }
            textIndex++
        }
        return indices
    }
}