package com.ferick.tickets

import com.ferick.AbstractTest
import com.ferick.annotations.SolverType
import com.ferick.annotations.TestFilePath
import com.ferick.solvers.tickets.CombinationTicketSolver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@TestFilePath("src/test/resources/tickets")
@SolverType(CombinationTicketSolver::class)
internal class TicketTest : AbstractTest() {

    @ParameterizedTest(name = "ticket test with input {0} and output {1}")
    @MethodSource("fileProvider")
    fun ticketTestWithParams(input: List<String>, output: String) {
        val result = solvers[CombinationTicketSolver::class]!!.solve(input)
        assertEquals(output, result)
    }
}