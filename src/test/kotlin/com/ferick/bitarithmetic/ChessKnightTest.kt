package com.ferick.bitarithmetic

import com.ferick.annotations.SolverType
import com.ferick.annotations.TestFilePath
import com.ferick.solvers.bitarithmetic.ChessKnightSolver
import org.junit.jupiter.api.Assertions.assertLinesMatch
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@TestFilePath("src/test/resources/bitarithmetic/knight")
@SolverType(ChessKnightSolver::class)
internal class ChessKnightTest : AbstractChessTest() {

    @ParameterizedTest(name = "chess knight test with input {0} and output {1}")
    @MethodSource("chessFileProvider")
    fun chessKnightTest(input: List<String>, output: List<String>) {
        val result = solvers[ChessKnightSolver::class]!!.solve(input).split(",")
        assertLinesMatch(output, result)
    }
}