package com.ferick.bitarithmetic

import com.ferick.annotations.SolverType
import com.ferick.annotations.TestFilePath
import com.ferick.solvers.bitarithmetic.ChessQueenSolver
import org.junit.jupiter.api.Assertions.assertLinesMatch
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@TestFilePath("src/test/resources/bitarithmetic/queen")
@SolverType(ChessQueenSolver::class)
internal class ChessQueenTest : AbstractChessTest() {

    @ParameterizedTest(name = "chess queen test with input {0} and output {1}")
    @MethodSource("chessFileProvider")
    fun chessQueenTest(input: List<String>, output: List<String>) {
        val result = solvers[ChessQueenSolver::class]!!.solve(input).split(",")
        assertLinesMatch(output, result)
    }
}