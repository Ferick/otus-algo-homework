package com.ferick.bitarithmetic

import com.ferick.annotations.SolverType
import com.ferick.annotations.TestFilePath
import com.ferick.solvers.bitarithmetic.ChessBishopSolver
import org.junit.jupiter.api.Assertions.assertLinesMatch
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@TestFilePath("src/test/resources/bitarithmetic/bishop")
@SolverType(ChessBishopSolver::class)
internal class ChessBishopTest : AbstractChessTest() {

    @ParameterizedTest(name = "chess bishop test with input {0} and output {1}")
    @MethodSource("chessFileProvider")
    fun chessBishopTest(input: List<String>, output: List<String>) {
        val result = solvers[ChessBishopSolver::class]!!.solve(input).split(",")
        assertLinesMatch(output, result)
    }
}