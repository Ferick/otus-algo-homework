package com.ferick.bitarithmetic

import com.ferick.annotations.SolverType
import com.ferick.annotations.TestFilePath
import com.ferick.solvers.bitarithmetic.ChessRookSolver
import org.junit.jupiter.api.Assertions.assertLinesMatch
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@TestFilePath("src/test/resources/bitarithmetic/rook")
@SolverType(ChessRookSolver::class)
internal class ChessRookTest : AbstractChessTest() {

    @ParameterizedTest(name = "chess rook test with input {0} and output {1}")
    @MethodSource("chessFileProvider")
    fun chessRookTest(input: List<String>, output: List<String>) {
        val result = solvers[ChessRookSolver::class]!!.solve(input).split(",")
        assertLinesMatch(output, result)
    }
}