package com.ferick.bitarithmetic

import com.ferick.annotations.SolverType
import com.ferick.annotations.TestFilePath
import com.ferick.solvers.bitarithmetic.ChessKingSolver
import org.junit.jupiter.api.Assertions.assertLinesMatch
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@TestFilePath("src/test/resources/bitarithmetic/king")
@SolverType(ChessKingSolver::class)
internal class ChessKingTest : AbstractChessTest() {

    @ParameterizedTest(name = "chess king test with input {0} and output {1}")
    @MethodSource("chessFileProvider")
    fun chessKingTest(input: List<String>, output: List<String>) {
        val result = solvers[ChessKingSolver::class]!!.solve(input).split(",")
        assertLinesMatch(output, result)
    }
}