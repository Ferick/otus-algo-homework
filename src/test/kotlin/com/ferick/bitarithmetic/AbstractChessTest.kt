package com.ferick.bitarithmetic

import com.ferick.AbstractTest
import org.junit.jupiter.params.provider.Arguments
import java.nio.file.Files

internal abstract class AbstractChessTest : AbstractTest() {

    protected fun chessFileProvider(): List<Arguments> {
        return pairs.map {
            Arguments.of(
                Files.readAllLines(it.first),
                Files.readAllLines(it.second)
            )
        }
    }
}