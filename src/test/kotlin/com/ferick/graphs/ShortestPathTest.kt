package com.ferick.graphs

import com.ferick.structures.graphs.AdjacencyVector
import com.ferick.structures.graphs.VectorItem
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

private val expectedVertices = mapOf(
    0 to 1,
    1 to 0,
    2 to 4,
    3 to 6,
    4 to 3,
    5 to 3,
    6 to 1
)

internal class ShortestPathTest {

    @Test
    fun shortestPathSearchTest() {
        val graph = getGraph()
        val search = ShortestPathSearch(graph)
        val vertexCosts = search.getVertexCosts(1)
        expectedVertices.forEach { (vertex, cost) ->
            assertEquals(cost, vertexCosts[vertex])
        }
    }

    private fun getGraph(): AdjacencyVector {
        val graph = AdjacencyVector(7)
        graph.addVectorItems(0,
            VectorItem(1, 1),
            VectorItem(2, 3),
            VectorItem(4, 2)
        )
        graph.addVectorItems(1,
            VectorItem(0, 1),
            VectorItem(2, 4),
            VectorItem(3, 6),
            VectorItem(6, 1)
        )
        graph.addVectorItems(2,
            VectorItem(0, 3),
            VectorItem(1, 4),
            VectorItem(3, 4),
            VectorItem(4, 2),
            VectorItem(5, 3)
        )
        graph.addVectorItems(3,
            VectorItem(1, 6),
            VectorItem(2, 4),
            VectorItem(5, 5)
        )
        graph.addVectorItems(4,
            VectorItem(0, 2),
            VectorItem(2, 2),
            VectorItem(6, 3)
        )
        graph.addVectorItems(5,
            VectorItem(2, 3),
            VectorItem(3, 5),
            VectorItem(6, 2)
        )
        graph.addVectorItems(6,
            VectorItem(1, 1),
            VectorItem(4, 3),
            VectorItem(5, 2)
        )
        return graph
    }
}