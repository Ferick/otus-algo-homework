package com.ferick.graphs

import com.ferick.structures.graphs.AdjacencyVector
import com.ferick.structures.graphs.VectorItem
import org.junit.jupiter.api.Assertions.assertIterableEquals
import org.junit.jupiter.api.Test

private val expectedVertices = listOf(4, 0, 1, 7, 3, 2, 5, 6)
private val expectedComponents = listOf(
    listOf(6, 5), listOf(2, 3, 7), listOf(1, 0, 4)
)

internal class TopologySearchTest {

    @Test
    fun searchTest() {
        val graph = getFirstExampleGraph()
        val invertedGraph = graph.invertDirections()
        val topologySearch = TopologySearch(invertedGraph)
        val result = topologySearch.search(6)
        assertIterableEquals(expectedVertices, result)
    }

    @Test
    fun componentSearchTest() {
        val graph = getFirstExampleGraph()
        val invertedGraph = graph.invertDirections()
        val topologySearch = TopologySearch(invertedGraph)
        val vertexList = topologySearch.search(6)
        val connectedSearch = StronglyConnectedComponentSearch(graph, vertexList)
        val result = connectedSearch.search()
        assertIterableEquals(expectedComponents, result)
    }

    @Test
    fun anotherSearchTest() {
        val graph = getSecondExampleGraph()
        val topologySearch = AnotherTopologySearch(graph)
        val result = topologySearch.search()
        assertIterableEquals(listOf(2, 3, 0, 5, 1, 4, 6, 7), result)
    }

    private fun getFirstExampleGraph(): AdjacencyVector {
        val graph = AdjacencyVector(8)
        graph.addVectorItems(0, VectorItem(1))
        graph.addVectorItems(1, VectorItem(2), VectorItem(4), VectorItem(5))
        graph.addVectorItems(2, VectorItem(3), VectorItem(6))
        graph.addVectorItems(3, VectorItem(2), VectorItem(7))
        graph.addVectorItems(4, VectorItem(0), VectorItem(5))
        graph.addVectorItems(5, VectorItem(6))
        graph.addVectorItems(6, VectorItem(5))
        graph.addVectorItems(7, VectorItem(3), VectorItem(6))
        return graph
    }

    private fun getSecondExampleGraph(): AdjacencyVector {
        val graph = AdjacencyVector(8)
        graph.addVectorItems(0, VectorItem(1))
        graph.addVectorItems(1, VectorItem(4))
        graph.addVectorItems(2, VectorItem(3))
        graph.addVectorItems(3,
            VectorItem(0), VectorItem(1), VectorItem(4), VectorItem(5)
        )
        graph.addVectorItems(4, VectorItem(6))
        graph.addVectorItems(5, VectorItem(7))
        graph.addVectorItems(6, VectorItem(7))
        graph.addVectorItems(7)
        return graph
    }
}