package com.ferick.graphs

import com.ferick.structures.graphs.AdjacencyVector
import com.ferick.structures.graphs.VectorItem
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

private val expectedWeights = listOf(1, 1, 1, 1, 2, 2, 3)

internal class MinimumSpanningTreeTest {

    @Test
    fun searchMinimumSpanningTreeTest() {
        val graph = getGraph()
        val edges = graph.edgesFromNonDirectedGraph
        val search = MinimumSpanningTreeSearch(graph, edges)
        val minimumSpanningTree = search.getMinimumSpanningTree()
        expectedWeights.forEachIndexed { index, weight ->
            assertEquals(weight, minimumSpanningTree.get(index).weight)
        }
    }

    private fun getGraph(): AdjacencyVector {
        val graph = AdjacencyVector(8)
        graph.addVectorItems(0,
            VectorItem(1, 1),
            VectorItem(7, 2),
            VectorItem(3, 6)
        )
        graph.addVectorItems(1,
            VectorItem(0, 1),
            VectorItem(7, 3),
            VectorItem(4, 4)
        )
        graph.addVectorItems(2,
            VectorItem(3, 1),
            VectorItem(4, 4),
            VectorItem(5, 1),
            VectorItem(6, 2),
            VectorItem(7, 5)
        )
        graph.addVectorItems(3,
            VectorItem(0, 6),
            VectorItem(2, 1),
            VectorItem(6, 4)
        )
        graph.addVectorItems(4,
            VectorItem(1, 4),
            VectorItem(2, 4),
            VectorItem(5, 3),
            VectorItem(7, 1)
        )
        graph.addVectorItems(5,
            VectorItem(2, 1),
            VectorItem(4, 3),
            VectorItem(6, 3)
        )
        graph.addVectorItems(6,
            VectorItem(2, 2),
            VectorItem(3, 4),
            VectorItem(5, 3)
        )
        graph.addVectorItems(7,
            VectorItem(0, 2),
            VectorItem(1, 3),
            VectorItem(2, 5),
            VectorItem(4, 1)
        )
        return graph
    }
}