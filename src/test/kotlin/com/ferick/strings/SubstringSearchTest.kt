package com.ferick.strings

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

internal class SubstringSearchTest {

    @ParameterizedTest(name = "search substring $MASK in T {1}")
    @MethodSource("textProvider")
    fun searchTest(text: String, textTimes: Int) {
        val substringSearch = SubstringSearch()
        val indices = substringSearch.findIndices(text, MASK)
        assertEquals(textTimes, indices.size)
        assertEquals(countLastIndex(textTimes), indices.last())
    }

    companion object {
        private const val TEXT = "STRONGSTRING"
        private const val MASK = "RING"
        private const val FIRST_MASK_INDEX_IN_TEXT = 8

        private fun multiplyText(times: Int) = (1..times).map { TEXT }.reduce { acc, s -> "$acc$s" }

        private fun countLastIndex(times: Int) =
            if (times == 1) {
                FIRST_MASK_INDEX_IN_TEXT
            } else {
                ((times - 1) * TEXT.length) + FIRST_MASK_INDEX_IN_TEXT
            }

        @JvmStatic
        fun textProvider(): List<Arguments> {
            return listOf(
                Arguments.of(multiplyText(1), 1),
                Arguments.of(multiplyText(10), 10),
                Arguments.of(multiplyText(100), 100),
                Arguments.of(multiplyText(1000), 1000)
            )
        }
    }
}