package com.ferick.structures

import com.ferick.structures.queues.priority.Priority
import com.ferick.structures.queues.priority.PriorityQueue
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class PriorityQueueTest {

    @Test
    fun takeByPriorityTest() {
        val priorityQueue = PriorityQueue<Priority, String>(Priority.values())

        priorityQueue.enqueue(Priority.FOURTH, "Ботаник")
        priorityQueue.enqueue(Priority.FOURTH, "Застенчивая девушка")
        priorityQueue.enqueue(Priority.THIRD, "Благообразный старичок")
        priorityQueue.enqueue(Priority.THIRD, "Толстый дядька")
        priorityQueue.enqueue(Priority.SECOND, "Настоящий мужик")
        priorityQueue.enqueue(Priority.SECOND, "Настоящая баба")
        priorityQueue.enqueue(Priority.FIRST, "Бабка")
        priorityQueue.enqueue(Priority.FIRST, "Скандальная тетка")

        assertEquals("Бабка", priorityQueue.dequeue())
        assertEquals("Скандальная тетка", priorityQueue.dequeue())
        assertEquals("Настоящий мужик", priorityQueue.dequeue())
        assertEquals("Настоящая баба", priorityQueue.dequeue())
        assertEquals("Благообразный старичок", priorityQueue.dequeue())
        assertEquals("Толстый дядька", priorityQueue.dequeue())
        assertEquals("Ботаник", priorityQueue.dequeue())
        assertEquals("Застенчивая девушка", priorityQueue.dequeue())
    }
}