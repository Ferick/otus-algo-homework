package com.ferick.structures.tries

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class TrieTest {

    private val trie = Trie<Int>().apply {
        put("cat", 1)
        put("cars", 2)
        put("d", 3)
        put("dog", 4)
    }

    @ParameterizedTest(name = "trie get test key {0} expected value {1}")
    @MethodSource("keyValueProvider")
    fun trieGetTest(key: String, expectedValue: Int?) {
        assertEquals(expectedValue, trie.get(key))
    }

    private fun keyValueProvider(): List<Arguments> {
        return listOf(
            Arguments.of("c", null),
            Arguments.of("ca", null),
            Arguments.of("cat", 1),
            Arguments.of("cats", null),
            Arguments.of("car", null),
            Arguments.of("cars", 2),
            Arguments.of("d", 3),
            Arguments.of("do", null),
            Arguments.of("dog", 4)
        )
    }
}