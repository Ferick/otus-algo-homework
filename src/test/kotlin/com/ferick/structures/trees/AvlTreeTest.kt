package com.ferick.structures.trees

import com.ferick.structures.trees.avl.AvlTree
import org.apache.commons.lang3.RandomStringUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

private val random = Random()

internal class AvlTreeTest {

    @Test
    fun sizeTest() {
        val tree = AvlTree<Int, String>()
        randomInts().forEach {
            tree.put(it, generateStringValue())
        }
        assertEquals(10000, tree.size())
    }

    @Test
    fun getTest() {
        val tree = AvlTree<Int, String>()
        val randomInts = randomInts()
        val firstEvenInts = randomInts.filter { it % 2 == 0 }.take(1000)
        randomInts.forEach {
            tree.put(it, generateStringValue())
        }
        val values = firstEvenInts.map { tree.get(it) }
        assertEquals(1000, values.size)
    }

    @Test
    fun removeTest() {
        val tree = AvlTree<Int, String>()
        val randomInts = randomInts()
        randomInts.forEach {
            tree.put(it, generateStringValue())
        }
        val toBeRemoved = randomInts.take(1000).toSet()
        toBeRemoved.forEach {
            tree.delete(it)
        }
        assertEquals(10000 - toBeRemoved.size, tree.size())
    }

    private fun randomInts(): List<Int> {
        return (1..10000).map { _ ->
            random.nextInt(999)
        }.toList()
    }

    private fun generateStringValue() = RandomStringUtils.randomAlphanumeric(8)
}
