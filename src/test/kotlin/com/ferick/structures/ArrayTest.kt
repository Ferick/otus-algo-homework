package com.ferick.structures

import com.ferick.structures.arrays.BasicArray
import com.ferick.structures.arrays.FactorArray
import com.ferick.structures.arrays.SingleArray
import com.ferick.structures.arrays.VectorArray
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

internal class ArrayTest {

    @ParameterizedTest(name = "add by index test with array {0}")
    @MethodSource("arrayProvider")
    fun addByIndexTest(array: BasicArray<Int>) {
        add(array, 10)
        array.add(7, 11)
        array.add(8, 12)
        assertEquals(12, array.size())
        assertEquals(12, array.get(8))
        assertEquals(10, array.get(11))
    }

    @ParameterizedTest(name = "remove by index test with array {0}")
    @MethodSource("arrayProvider")
    fun removeByIndexTest(array: BasicArray<Int>) {
        add(array, 10)
        val removedBy7 = array.remove(7)
        val removedBy8 =array.remove(8)
        assertEquals(8, array.size())
        assertEquals(8, removedBy7)
        assertEquals(10, removedBy8)
        assertEquals(9, array.get(7))
    }

    private fun add(array: BasicArray<Int>, range: Int) {
        (1..range).forEach {
            array.add(it)
        }
    }

    companion object {
        @JvmStatic
        fun arrayProvider(): List<Arguments> {
            return listOf(
                Arguments.of(SingleArray<Int>()),
                Arguments.of(VectorArray<Int>()),
                Arguments.of(FactorArray<Int>())
            )
        }
    }
}