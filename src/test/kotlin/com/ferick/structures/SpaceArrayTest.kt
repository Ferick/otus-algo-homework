package com.ferick.structures

import com.ferick.structures.arrays.space.SpaceArray
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class SpaceArrayTest {

    @Test
    fun addTest() {
        val array = SpaceArray<Int>()
        add(array, 1010);
        assertEquals(1010, array.size())
        assertEquals(1, array.get(0))
        assertEquals(100, array.get(99))
        assertEquals(101, array.get(100))
        assertEquals(102, array.get(101))
        assertEquals(750, array.get(749))
        assertEquals(851, array.get(850))
        assertEquals(852, array.get(851))
        assertEquals(976, array.get(975))
        assertEquals(1001, array.get(1000))
        assertEquals(1010, array.get(1009))
    }

    @Test
    fun addByIndexTest() {
        val array = SpaceArray<Int>()
        add(array, 150);
        (1..150).forEach {
            array.add(49, it)
        }
        assertEquals(300, array.size())
        assertEquals(150, array.get(49))
        assertEquals(150, array.get(299))
    }

    @Test
    fun removeByIndexTest() {
        val array = SpaceArray<Int>()
        add(array, 150);
        val removeBy0 = array.remove(0)
        val removeBy50 = array.remove(50)
        val removeBy99 = array.remove(99)
        val removeBy100 = array.remove(100)
        val removeBy101 = array.remove(101)
        val removeBy144 = array.remove(144)
        assertEquals(1, removeBy0)
        assertEquals(52, removeBy50)
        assertEquals(102, removeBy99)
        assertEquals(104, removeBy100)
        assertEquals(106, removeBy101)
        assertEquals(150, removeBy144)
    }

    private fun add(array: SpaceArray<Int>, range: Int) {
        (1..range).forEach {
            array.add(it)
        }
    }
}