package com.ferick.structures.maps

import org.apache.commons.lang3.RandomStringUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

internal class SimpleHashMapTest {

    @Test
    fun sizeTest() {
        val simpleHashMap = SimpleHashMap<String, String>()
        addEntries(simpleHashMap, 100)
        assertEquals(100, simpleHashMap.size())
    }

    @Test
    fun getTest() {
        val simpleHashMap = SimpleHashMap<String, String>()
        addEntries(simpleHashMap, 50)
        simpleHashMap.put("key_25", "value_25")
        assertEquals("value_25", simpleHashMap.get("key_25"))
        assertNull(simpleHashMap.get("key_100"))
    }

    @Test
    fun removeTest() {
        val simpleHashMap = SimpleHashMap<String, String>()
        addEntries(simpleHashMap, 50)
        simpleHashMap.remove("key_50")
        assertEquals(49, simpleHashMap.size())
        assertNull(simpleHashMap.get("key_50"))
    }

    private fun addEntries(map: SimpleHashMap<String, String>, count: Int) {
        (1..count).forEach {
            map.put("key_$it", generateStringValue())
        }
    }

    private fun generateStringValue() = RandomStringUtils.randomAlphanumeric(8)
}