package com.ferick

import com.ferick.annotations.SolverType
import com.ferick.annotations.TestFilePath
import com.ferick.helpers.getTestFilesFromDirectory
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.provider.Arguments
import java.lang.Exception
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.reflect.KClass

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal abstract class AbstractTest {

    protected val solvers: Map<KClass<out Solver>, Solver> = this.javaClass.getAnnotation(SolverType::class.java)?.let {
        listOf(it.value).plus(it.values).associate { clazz ->
            try {
                val constructor = clazz.java.getConstructor()
                val instance = constructor.newInstance()
                Pair(clazz, instance)
            } catch (e: Exception) {
                throw IllegalStateException("Solver must have constructor without arguments")
            }
        }
    } ?: throw IllegalStateException("Test class must be annotated with @SolverType")

    protected val pairs = this.javaClass.getAnnotation(TestFilePath::class.java)?.let {
        getTestFilesFromDirectory(Paths.get(it.value))
    } ?: throw IllegalStateException("Test class must be annotated with @TestFilePath")

    protected fun fileProvider(): List<Arguments> {
        return pairs.map {
            Arguments.of(
                Files.readAllLines(it.first),
                Files.readString(it.second).trim()
            )
        }
    }
}