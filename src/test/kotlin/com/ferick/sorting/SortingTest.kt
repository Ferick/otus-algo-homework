package com.ferick.sorting

import com.ferick.AbstractTest
import com.ferick.annotations.SolverType
import com.ferick.annotations.TestFilePath
import com.ferick.solvers.sorting.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.nio.file.Files

@TestFilePath("src/test/resources/sorting/random")
@SolverType(
    BubbleSortSolver::class,
    InsertionSortSolver::class,
    ShellSortSolver::class,
    SelectionSortSolver::class,
    HeapSortSolver::class,
    QuickSortSolver::class,
    MergeSortSolver::class,
    CountingSortSolver::class,
    RadixSortSolver::class,
    BucketSortSolver::class
)
internal class SortingTest : AbstractTest() {

    @ParameterizedTest(name = "bubble sort test with array size {0}")
    @MethodSource("sortFileProvider")
    fun bubbleSortTest(size: Int, input: String, output: String) {
        val result = solvers[BubbleSortSolver::class]!!.solve(listOf(input))
        assertEquals(output, result)
    }

    @ParameterizedTest(name = "insertion sort test with array size {0}")
    @MethodSource("sortFileProvider")
    fun insertionSortTest(size: Int, input: String, output: String) {
        val result = solvers[InsertionSortSolver::class]!!.solve(listOf(input))
        assertEquals(output, result)
    }

    @ParameterizedTest(name = "shell sort test with array size {0}")
    @MethodSource("sortFileProvider")
    fun shellSortTest(size: Int, input: String, output: String) {
        val result = solvers[ShellSortSolver::class]!!.solve(listOf(input))
        assertEquals(output, result)
    }

    @ParameterizedTest(name = "selection sort test with array size {0}")
    @MethodSource("sortFileProvider")
    fun selectionSortTest(size: Int, input: String, output: String) {
        val result = solvers[SelectionSortSolver::class]!!.solve(listOf(input))
        assertEquals(output, result)
    }

    @ParameterizedTest(name = "heap sort test with array size {0}")
    @MethodSource("sortFileProvider")
    fun heapSortTest(size: Int, input: String, output: String) {
        val result = solvers[HeapSortSolver::class]!!.solve(listOf(input))
        assertEquals(output, result)
    }

    @ParameterizedTest(name = "quick sort test with array size {0}")
    @MethodSource("sortFileProvider")
    fun quickSortTest(size: Int, input: String, output: String) {
        val result = solvers[QuickSortSolver::class]!!.solve(listOf(input))
        assertEquals(output, result)
    }

    @ParameterizedTest(name = "merge sort test with array size {0}")
    @MethodSource("sortFileProvider")
    fun mergeSortTest(size: Int, input: String, output: String) {
        val result = solvers[MergeSortSolver::class]!!.solve(listOf(input))
        assertEquals(output, result)
    }

    @ParameterizedTest(name = "counting sort test with array size {0}")
    @MethodSource("sortFileProvider")
    fun countingSortTest(size: Int, input: String, output: String) {
        val result = solvers[CountingSortSolver::class]!!.solve(listOf(input))
        assertEquals(output, result)
    }

    @ParameterizedTest(name = "radix sort test with array size {0}")
    @MethodSource("sortFileProvider")
    fun radixSortTest(size: Int, input: String, output: String) {
        val result = solvers[RadixSortSolver::class]!!.solve(listOf(input))
        assertEquals(output, result)
    }

    @ParameterizedTest(name = "bucket sort test with array size {0}")
    @MethodSource("shortSortFileProvider")
    fun bucketSortTest(size: Int, input: String, output: String) {
        val result = solvers[BucketSortSolver::class]!!.solve(listOf(input))
        assertEquals(output, result)
    }

    private fun sortFileProvider(): List<Arguments> {
        return pairs.map {
            val input = Files.readAllLines(it.first)
            Arguments.of(
                input[0].toInt(),
                input[1],
                Files.readString(it.second).trim()
            )
        }
    }

    private fun shortSortFileProvider(): List<Arguments> {
        return pairs.take(5).map {
            val input = Files.readAllLines(it.first)
            Arguments.of(
                input[0].toInt(),
                input[1],
                Files.readString(it.second).trim()
            )
        }
    }
}