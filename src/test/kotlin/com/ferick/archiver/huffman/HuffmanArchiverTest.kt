package com.ferick.archiver.huffman

import com.ferick.helpers.extensions.toPath
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths

@ExperimentalUnsignedTypes
internal class HuffmanArchiverTest {

    private val archiver = HuffmanArchiver()
    private val tempDirectory = Files.createTempDirectory(Paths.get(""), "archives")

    @ParameterizedTest
    @MethodSource("textProvider")
    fun compressFile(filePath: String, expectedBytes: ByteArray) {
        val archiveFileName = tempDirectory.resolve(ARCHIVE_FILE_NAME)
        archiver.compressFile(filePath, archiveFileName.toString())
        val result = Files.readAllBytes(archiveFileName)
        assertArrayEquals(expectedBytes, result)
    }

    @ParameterizedTest
    @MethodSource("archiveProvider")
    fun decompressFile(filePath: String, expectedTextFilePath: String) {
        val dataFileName = tempDirectory.resolve(ARCHIVE_FILE_NAME)
        archiver.decompressFile(filePath, dataFileName.toString())
        val result = Files.readString(dataFileName, Charset.forName("Windows-1251"))
        val expectedText = Files.readString(expectedTextFilePath.toPath())
        assertEquals(expectedText, result)
    }

    @AfterEach
    fun tearDown() {
        tempDirectory.toFile().deleteRecursively()
    }

    companion object {
        private const val LAT_DATA_FILE_NAME = "src/test/resources/archiver/text_lat.txt"
        private const val CYR_DATA_FILE_NAME = "src/test/resources/archiver/text_cyr.txt"
        private const val ARCHIVE_FILE_NAME = "archive.txt"
        private const val LAT_ARCHIVE_FILE_NAME = "src/test/resources/archiver/archive_lat.txt"
        private const val CYR_ARCHIVE_FILE_NAME = "src/test/resources/archiver/archive_cyr.txt"

        private val latExpectedBytes = byteArrayOf(11, 0, 0, 0, 5, 65, 5, 66, 2, 68, 1, 75, 1, 82, 2, 118, 21, 59)
        private val cyrExpectedBytes = byteArrayOf(11, 0, 0, 0, 5, -32, 5, -31, 2, -28, 1, -22, 1, -16, 2, 118, 21, 59)

        @JvmStatic
        fun textProvider(): List<Arguments> {
            return listOf(
                Arguments.of(LAT_DATA_FILE_NAME, latExpectedBytes),
                Arguments.of(CYR_DATA_FILE_NAME, cyrExpectedBytes)
            )
        }

        @JvmStatic
        fun archiveProvider(): List<Arguments> {
            return listOf(
                Arguments.of(LAT_ARCHIVE_FILE_NAME, LAT_DATA_FILE_NAME),
                Arguments.of(CYR_ARCHIVE_FILE_NAME, CYR_DATA_FILE_NAME)
            )
        }
    }
}