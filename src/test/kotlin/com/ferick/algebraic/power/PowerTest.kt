package com.ferick.algebraic.power

import com.ferick.AbstractTest
import com.ferick.annotations.SolverType
import com.ferick.annotations.TestFilePath
import com.ferick.solvers.algebraic.power.BinaryMultiplyPowerSolver
import com.ferick.solvers.algebraic.power.DecompositionPowerSolver
import com.ferick.solvers.algebraic.power.IterablePowerSolver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@TestFilePath("src/test/resources/algebraic/power")
@SolverType(
    IterablePowerSolver::class,
    BinaryMultiplyPowerSolver::class,
    DecompositionPowerSolver::class
)
internal class PowerTest : AbstractTest() {

    @ParameterizedTest(name = "iterable power test with input {0} and output {1}")
    @MethodSource("fileProvider")
    fun iterablePowerTest(input: List<String>, output: String) {
        val result = solvers[IterablePowerSolver::class]!!.solve(input)
        assertEquals(output.toDouble(), result.toDouble(), 0.00000000001)
    }

    @ParameterizedTest(name = "binary multiply power test with input {0} and output {1}")
    @MethodSource("fileProvider")
    fun binaryMultiplyPowerTest(input: List<String>, output: String) {
        val result = solvers[BinaryMultiplyPowerSolver::class]!!.solve(input)
        assertEquals(output.toDouble(), result.toDouble(), 0.0000001)
    }

    @ParameterizedTest(name = "decomposition power test with input {0} and output {1}")
    @MethodSource("fileProvider")
    fun decompositionPowerTest(input: List<String>, output: String) {
        val result = solvers[DecompositionPowerSolver::class]!!.solve(input)
        assertEquals(output.toDouble(), result.toDouble(), 0.0000001)
    }
}
