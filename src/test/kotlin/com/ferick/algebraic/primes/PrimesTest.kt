package com.ferick.algebraic.primes

import com.ferick.AbstractTest
import com.ferick.annotations.SolverType
import com.ferick.annotations.TestFilePath
import com.ferick.solvers.algebraic.primes.PrimesSolver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@TestFilePath("src/test/resources/algebraic/primes")
@SolverType(PrimesSolver::class)
internal class PrimesTest : AbstractTest() {

    @ParameterizedTest(name = "primes test with input {0} and output {1}")
    @MethodSource("fileProvider")
    fun primesTest(input: List<String>, output: String) {
        val result = solvers[PrimesSolver::class]!!.solve(input)
        assertEquals(output, result)
    }
}