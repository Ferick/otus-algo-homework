package com.ferick.algebraic.fibo

import com.ferick.AbstractTest
import com.ferick.annotations.SolverType
import com.ferick.annotations.TestFilePath
import com.ferick.solvers.algebraic.fibo.GoldenRatioFibonacciSolver
import com.ferick.solvers.algebraic.fibo.IterableFibonacciSolver
import com.ferick.solvers.algebraic.fibo.MatrixFibonacciSolver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.nio.file.Files

@TestFilePath("src/test/resources/algebraic/fibo")
@SolverType(
    IterableFibonacciSolver::class,
    GoldenRatioFibonacciSolver::class,
    MatrixFibonacciSolver::class
)
internal class FibonacciTest : AbstractTest() {

    @ParameterizedTest(name = "iterable fibonacci test with input {0} and output {1}")
    @MethodSource("fileProvider")
    fun iterableFibonacciTest(input: List<String>, output: String) {
        val result = solvers[IterableFibonacciSolver::class]!!.solve(input)
        assertEquals(output, result)
    }

    @ParameterizedTest(name = "golden ratio fibonacci test with input {0} and output {1}")
    @MethodSource("shortFileProvider")
    fun goldenRatioFibonacciTest(input: List<String>, output: String) {
        val result = solvers[GoldenRatioFibonacciSolver::class]!!.solve(input)
        assertEquals(output, result)
    }

    @ParameterizedTest(name = "matrix fibonacci test with input {0} and output {1}")
    @MethodSource("fileProvider")
    fun matrixFibonacciTest(input: List<String>, output: String) {
        val result = solvers[MatrixFibonacciSolver::class]!!.solve(input)
        assertEquals(output, result)
    }

    private fun shortFileProvider(): List<Arguments> {
        return pairs.take(7).map {
            Arguments.of(
                Files.readAllLines(it.first),
                Files.readString(it.second).trim()
            )
        }
    }
}